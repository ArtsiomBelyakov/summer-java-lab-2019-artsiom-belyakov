package by.epam.training;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public final class Date {
    private LocalDate nowDate = LocalDate.now();
    private LocalTime nowTime = LocalTime.now();

    private String day_month;
    private String seconds_hour;
    private String year;
    private String minutes;
    private String day_month_add_week;


    public Date() {
        day_month = nowDate.format(DateTimeFormatter.ofPattern("dd MMMM ")); // 26 Jun
        seconds_hour = nowTime.format(DateTimeFormatter.ofPattern("ss:hh ")); // 08:10
        year = nowDate.format(DateTimeFormatter.ofPattern("yyyy ")); // 2019
        minutes = nowTime.format(DateTimeFormatter.ofPattern("mm")); // 20
        day_month_add_week=nowDate.plusWeeks(1).format(DateTimeFormatter.ofPattern("dd MMMM ")); // 26 Jun + 1 week
    }

    public String getDay_month() {
        return day_month;
    }

    public String getSeconds_hour() {
        return seconds_hour;
    }

    public String getYear() {
        return year;
    }

    public String getMinutes() {
        return minutes;
    }

    public String getDay_month_add_week() {
        return day_month_add_week;
    }
}
