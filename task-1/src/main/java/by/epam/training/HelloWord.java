package by.epam.training;

//Hello world: (число) (месяц в виде строки) (секунды) (час) (год) (минуты)
//Hello world + 1 week: (число + 1 неделя) (месяц в виде строки) (секунды) (час) (год) (минуты)

public class HelloWord {
    public static void main(String[] args) {

        Date date = new Date();

        System.out.println("Hello word: " + date.getDay_month() +
                date.getSeconds_hour() + date.getYear() + date.getMinutes());

        System.out.println("Hello world + 1 week: " + (date.getDay_month_add_week()) +
                date.getSeconds_hour() + date.getYear() + date.getMinutes());
    }


}
