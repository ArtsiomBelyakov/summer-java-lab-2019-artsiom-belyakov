package by.epam.training.db;

import by.epam.training.db.dao.jdbc.MySqlClient;
import by.epam.training.db.dao.jdbc.MySqlEmployee;
import by.epam.training.db.dao.jdbc.MySqlSale;
import by.epam.training.db.entity.Client;
import by.epam.training.db.entity.Employee;

import java.util.stream.Stream;

public class Run {
    public static void main(String[] args) {
        MySqlClient sqlClient = new MySqlClient();
        sqlClient.getAll().stream().forEach(System.out::println);
        sqlClient.update(new Client(1, "Update", "Update", "Update"));
        sqlClient.insert(new Client("Insert","Insert","Insert"));
        sqlClient.delete(3);
        System.out.println("\nCRUD operations");
        sqlClient.getAll().stream().forEach(System.out::println);
        System.out.println("\nSELECT id=1");
        Stream.of(sqlClient.select(1)).forEach(System.out::println);
        System.out.println('\n');

        MySqlEmployee sqlEmployee = new MySqlEmployee();
        sqlEmployee.getAll().stream().forEach(System.out::println);
        sqlEmployee.update(new Employee(2, 2,"EmpUpd","EmpUpd","EmpUpd"));
        sqlEmployee.insert(new Employee(3,"InsEmp","InsEmp","InsEmp"));
        sqlEmployee.delete(3);
        System.out.println("\nCRUD operations");
        sqlEmployee.getAll().stream().forEach(System.out::println);
        System.out.println("\nSELECT id=5");
        Stream.of(sqlEmployee.select(5)).forEach(System.out::println);

        System.out.println("\nJOIN");
        Stream.of(new MySqlSale().join(3)).forEach(System.out::println);
    }
}
