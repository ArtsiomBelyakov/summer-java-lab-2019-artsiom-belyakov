package by.epam.training.db.dao;

public interface GenericDAO<E> {
    boolean insert(E entity);
    E select(int id);
    boolean update(E element);
    void delete(int id);
}