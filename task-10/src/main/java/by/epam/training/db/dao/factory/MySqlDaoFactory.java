package by.epam.training.db.dao.factory;

import javax.sql.DataSource;

import by.epam.training.db.configuration.ConfigurationManager;
import org.apache.commons.dbcp.BasicDataSource;

public class MySqlDaoFactory {
    private static BasicDataSource dataSource;

    static {
        dataSource = new BasicDataSource();
        dataSource.setDriverClassName(ConfigurationManager.getProperty("db.driver"));
        dataSource.setUrl(ConfigurationManager.getProperty("db.url"));
        dataSource.setUsername(ConfigurationManager.getProperty("db.user"));
        dataSource.setPassword(ConfigurationManager.getProperty("db.password"));

        dataSource.setMinIdle(10);
        dataSource.setMaxIdle(100);
    }
    public static DataSource getDataSource() {
        return dataSource;
    }
}