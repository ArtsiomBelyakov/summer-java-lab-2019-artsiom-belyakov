package by.epam.training.db.dao.jdbc;

import by.epam.training.db.configuration.ConfigurationManager;
import by.epam.training.db.dao.GenericDAO;
import by.epam.training.db.dao.factory.MySqlDaoFactory;
import by.epam.training.db.entity.Client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySqlClient implements GenericDAO<Client> {
    @Override
    public boolean insert(Client client) {
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement ps =
                     cn.prepareStatement(ConfigurationManager.getProperty("INSERT_CLIENT"))) {
            setClientPreparedStatement(client, ps);
            return (ps.executeUpdate() != 0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Client select(int id) {
        Client client = null;
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement statement =
                     cn.prepareStatement(ConfigurationManager.getProperty("SELECT_CLIENT_BY_ID"))) {
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                client = createClient(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return client;
    }

    @Override
    public boolean update(Client client) {
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement ps =
                     cn.prepareStatement(ConfigurationManager.getProperty("UPDATE_CLIENT"))) {
            setClientPreparedStatement(client, ps);
            ps.setInt(4, client.getId());
            return (ps.executeUpdate() != 0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void delete(int id) {
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement ps = cn.prepareStatement(ConfigurationManager.getProperty("DELETE_CLIENT_BY_ID"))) {
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List getAll() {
        List<Client> list = new ArrayList<>();
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement stm = cn.prepareStatement(ConfigurationManager.getProperty("SELECT_ALL_CLIENT"))) {
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Client client = createClient(rs);
                list.add(client);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    private void setClientPreparedStatement(Client client, PreparedStatement ps) {
        try {
            ps.setString(1, client.getFullname());
            ps.setString(2, client.getAddress());
            ps.setString(3, client.getPhone());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Client createClient(ResultSet rs) {
        try {
            Client client = new Client();
            client.setId(rs.getInt("id"));
            client.setFullname(rs.getString("fullname"));
            client.setAddress(rs.getString("address"));
            client.setPhone(rs.getString("phone"));
            return client;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
