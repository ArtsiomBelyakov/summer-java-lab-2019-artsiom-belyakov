package by.epam.training.db.dao.jdbc;

import by.epam.training.db.configuration.ConfigurationManager;
import by.epam.training.db.dao.GenericDAO;
import by.epam.training.db.dao.factory.MySqlDaoFactory;
import by.epam.training.db.entity.Employee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySqlEmployee implements GenericDAO<Employee> {
    @Override
    public boolean insert(Employee employee) {
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement ps =
                     cn.prepareStatement(ConfigurationManager.getProperty("INSERT_EMPLOYEE"))) {
            setEmployeePreparedStatement(employee, ps);
            return (ps.executeUpdate() != 0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Employee select(int id) {
        Employee employee = null;
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement statement =
                     cn.prepareStatement(ConfigurationManager.getProperty("SELECT_EMPLOYEE_BY_ID"))) {
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                employee = createEmployee(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return employee;
    }

    @Override
    public boolean update(Employee employee) {
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement ps =
                     cn.prepareStatement(ConfigurationManager.getProperty("UPDATE_EMPLOYEE"))) {
            setEmployeePreparedStatement(employee, ps);
            ps.setInt(5, employee.getId());
            return (ps.executeUpdate() != 0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void delete(int id) {
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement ps = cn.prepareStatement(ConfigurationManager.getProperty("DELETE_EMPLOYEE_BY_ID"))) {
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List getAll() {
        List<Employee> list = new ArrayList<>();
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement stm = cn.prepareStatement(ConfigurationManager.getProperty("SELECT_ALL_EMPLOYEE"))) {
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Employee employee = createEmployee(rs);
                list.add(employee);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    private void setEmployeePreparedStatement(Employee employee, PreparedStatement ps) {
        try {
            ps.setInt(1,employee.getPosition_id());
            ps.setString(2, employee.getName());
            ps.setString(3, employee.getSurname());
            ps.setString(4, employee.getPatronymic());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Employee createEmployee(ResultSet rs) {
        try {
            Employee employee = new Employee();
            employee.setId(rs.getInt("id"));
            employee.setPosition_id(rs.getInt("position_id"));
            employee.setSurname(rs.getString("surname"));
            employee.setName(rs.getString("name"));
            employee.setPatronymic(rs.getString("patronymic"));
            return employee;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}