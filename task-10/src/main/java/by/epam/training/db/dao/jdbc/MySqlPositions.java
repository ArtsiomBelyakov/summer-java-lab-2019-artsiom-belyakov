package by.epam.training.db.dao.jdbc;

import by.epam.training.db.configuration.ConfigurationManager;
import by.epam.training.db.dao.GenericDAO;
import by.epam.training.db.dao.factory.MySqlDaoFactory;
import by.epam.training.db.entity.Positions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySqlPositions implements GenericDAO<Positions> {

    @Override
    public boolean insert(Positions positions) {
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement ps =
                     cn.prepareStatement(ConfigurationManager.getProperty("INSERT_POSITIONS"))) {
            setPositionsPreparedStatement(positions, ps);
            return (ps.executeUpdate() != 0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Positions select(int id) {
        Positions positions = null;
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement statement =
                     cn.prepareStatement(ConfigurationManager.getProperty("SELECT_POSITIONS_BY_ID"))) {
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                positions = createPositions(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return positions;
    }

    @Override
    public boolean update(Positions positions) {
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement ps =
                     cn.prepareStatement(ConfigurationManager.getProperty("UPDATE_POSITIONS"))) {
            setPositionsPreparedStatement(positions, ps);
            ps.setInt(2, positions.getId());
            return (ps.executeUpdate() != 0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void delete(int id) {
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement ps = cn.prepareStatement(ConfigurationManager.getProperty("DELETE_POSITIONS_BY_ID"))) {
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List getAll() {
        List<Positions> list = new ArrayList<>();
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement stm = cn.prepareStatement(ConfigurationManager.getProperty("SELECT_ALL_POSITIONS"))) {
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Positions positions = createPositions(rs);
                list.add(positions);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    private void setPositionsPreparedStatement(Positions positions, PreparedStatement ps) {
        try {
            ps.setInt(1, positions.getId());
            ps.setString(2, positions.getPosition());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Positions createPositions(ResultSet rs) {
        try {
            Positions positions = new Positions();
            positions.setId(rs.getInt("id"));
            positions.setPosition(rs.getString("dateSale"));
            return positions;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}