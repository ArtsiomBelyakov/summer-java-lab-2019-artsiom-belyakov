package by.epam.training.db.dao.jdbc;

import by.epam.training.db.configuration.ConfigurationManager;
import by.epam.training.db.dao.GenericDAO;
import by.epam.training.db.dao.factory.MySqlDaoFactory;
import by.epam.training.db.entity.Product;
import by.epam.training.db.entity.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySqlProduct implements GenericDAO<Product> {
    @Override
    public boolean insert(Product product) {
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement ps =
                     cn.prepareStatement(ConfigurationManager.getProperty("INSERT_PRODUCT"))) {
            setProductPreparedStatement(product, ps);
            return (ps.executeUpdate() != 0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Product select(int id) {
        Product product = null;
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement statement =
                     cn.prepareStatement(ConfigurationManager.getProperty("SELECT_PRODUCT_BY_ID"))) {
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                product = createProduct(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return product;
    }

    @Override
    public boolean update(Product product) {
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement ps =
                     cn.prepareStatement(ConfigurationManager.getProperty("UPDATE_PRODUCT"))) {
            setProductPreparedStatement(product, ps);
            ps.setInt(4, product.getId());
            return (ps.executeUpdate() != 0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void delete(int id) {
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement ps = cn.prepareStatement(ConfigurationManager.getProperty("DELETE_PRODUCT_BY_ID"))) {
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List getAll() {
        List<Product> list = new ArrayList<>();
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement stm = cn.prepareStatement(ConfigurationManager.getProperty("SELECT_ALL_PRODUCT"))) {
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Product product = createProduct(rs);
                list.add(product);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    private void setProductPreparedStatement(Product product, PreparedStatement ps) {
        try {
            ps.setString(1, product.getTitle());
            ps.setObject(2, product.getDate());
            ps.setInt(3, product.getPrice());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Product createProduct(ResultSet rs) {
        try {
            Product product = new Product();
            product.setId(rs.getInt("id"));
            product.setTitle(rs.getString("title"));
            product.setDate(rs.getDate("date").toLocalDate());
            product.setPrice(rs.getInt("price"));
            return product;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
