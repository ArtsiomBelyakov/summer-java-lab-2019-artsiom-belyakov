package by.epam.training.db.dao.jdbc;

import by.epam.training.db.configuration.ConfigurationManager;
import by.epam.training.db.dao.GenericDAO;
import by.epam.training.db.dao.factory.MySqlDaoFactory;
import by.epam.training.db.entity.Employee;
import by.epam.training.db.entity.Sale;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MySqlSale implements GenericDAO<Sale> {
    @Override
    public boolean insert(Sale sale) {
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement ps =
                     cn.prepareStatement(ConfigurationManager.getProperty("INSERT_SALE"))) {
            setSalePreparedStatement(sale, ps);
            return (ps.executeUpdate() != 0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Sale select(int id) {
        Sale sale = null;
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement statement =
                     cn.prepareStatement(ConfigurationManager.getProperty("SELECT_SALE_BY_ID"))) {
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                sale = createSale(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sale;
    }

    @Override
    public boolean update(Sale sale) {
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement ps =
                     cn.prepareStatement(ConfigurationManager.getProperty("UPDATE_SALE"))) {
            setSalePreparedStatement(sale, ps);
            ps.setInt(5, sale.getId());
            return (ps.executeUpdate() != 0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void delete(int id) {
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement ps = cn.prepareStatement(ConfigurationManager.getProperty("DELETE_SALE_BY_ID"))) {
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List getAll() {
        List<Sale> list = new ArrayList<>();
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement stm = cn.prepareStatement(ConfigurationManager.getProperty("SELECT_ALL_SALE"))) {
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Sale sale = createSale(rs);
                list.add(sale);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public String join(int id) {
        Sale sale;
        Employee employee = new Employee();
        try (Connection cn = MySqlDaoFactory.getDataSource().getConnection();
             PreparedStatement statement =
                     cn.prepareStatement(ConfigurationManager.getProperty("SELECT_JOIN_SALE_EMPLOYEE"))) {
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                sale = createSale(rs);
                employee.setId(rs.getInt("id"));
                employee.setPosition_id(rs.getInt("position_id"));
                employee.setSurname(rs.getString("surname"));
                employee.setName(rs.getString("name"));
                employee.setPatronymic(rs.getString("patronymic"));
                return sale + "\n" + employee;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "null";
    }

    private void setSalePreparedStatement(Sale sale, PreparedStatement ps) {
        try {
            ps.setObject(1, sale.getDateSale());
            ps.setInt(2, sale.getEmployee_id());
            ps.setInt(3, sale.getProduct_id());
            ps.setInt(4, sale.getClient_id());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Sale createSale(ResultSet rs) {
        try {
            Sale sale = new Sale();
            sale.setId(rs.getInt("id"));
            sale.setDateSale(rs.getDate("dateSale").toLocalDate());
            sale.setEmployee_id(rs.getInt("employee_id"));
            sale.setProduct_id(rs.getInt("product_id"));
            sale.setClient_id(rs.getInt("client_id"));
            return sale;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
