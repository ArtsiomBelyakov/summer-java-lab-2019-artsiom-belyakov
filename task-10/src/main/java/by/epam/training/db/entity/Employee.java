package by.epam.training.db.entity;

public class Employee {
    private int id;
    private int position_id;
    private String surname;
    private String name;
    private String patronymic;

    public Employee() {
    }

    public Employee(int id, int position_id, String surname, String name, String patronymic) {
        this.id = id;
        this.position_id = position_id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
    }

    public Employee(int position_id, String surname, String name, String patronymic) {
        this.position_id = position_id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPosition_id() {
        return position_id;
    }

    public void setPosition_id(int position_id) {
        this.position_id = position_id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", position_id=" + position_id +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", patronymic='" + patronymic + '\'' +
                '}';
    }
}