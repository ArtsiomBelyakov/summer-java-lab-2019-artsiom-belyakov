package by.epam.training.db.entity;

import java.time.LocalDate;

public class Product {
    private int id;
    private String title;
    private LocalDate date;
    private int price;

    public Product() {
    }

    public Product(int id, String title, LocalDate date, int price) {
        this.id = id;
        this.title = title;
        this.date = date;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", date=" + date +
                ", price=" + price +
                '}';
    }
}