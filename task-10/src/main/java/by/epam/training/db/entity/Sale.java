package by.epam.training.db.entity;

import java.time.LocalDate;

public class Sale {
    private int id;
    private LocalDate dateSale;
    private int employee_id;
    private int product_id;
    private int client_id;

    public Sale() {
    }

    public Sale(int id, LocalDate dateSale, int employee_id, int product_id, int client_id) {
        this.id = id;
        this.dateSale = dateSale;
        this.employee_id = employee_id;
        this.product_id = product_id;
        this.client_id = client_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getDateSale() {
        return dateSale;
    }

    public void setDateSale(LocalDate dateSale) {
        this.dateSale = dateSale;
    }

    public int getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(int employee_id) {
        this.employee_id = employee_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getClient_id() {
        return client_id;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    @Override
    public String toString() {
        return "Sale{" +
                "id=" + id +
                ", dateSale=" + dateSale +
                ", employee_id=" + employee_id +
                ", product_id=" + product_id +
                ", client_id=" + client_id +
                '}';
    }
}