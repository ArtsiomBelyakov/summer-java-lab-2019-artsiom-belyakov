CREATE DATABASE techx;
USE techx;
create table client
(
    id       int auto_increment
        primary key,
    fullName varchar(128) not null,
    address  varchar(100) not null,
    phone    varchar(32)  not null
);

create table positions
(
    id       int auto_increment
        primary key,
    position varchar(56) not null
);

create table employee
(
    id          int auto_increment
        primary key,
    position_id int         null,
    surname     varchar(56) not null,
    name        varchar(56) not null,
    patronymic  varchar(56) not null,
    constraint employee_position_id_fk
        foreign key (position_id) references positions (id)
            on update cascade on delete cascade
);

create table product
(
    id    int auto_increment
        primary key,
    title varchar(56) not null,
    date  date        not null,
    price int         not null
);

create table sale
(
    id          int auto_increment
        primary key,
    dateSale    date not null,
    employee_id int  null,
    product_id  int  null,
    client_id   int  null,
    constraint sale_client_id_fk
        foreign key (client_id) references client (id)
            on update cascade on delete cascade,
    constraint sale_employee_id_fk
        foreign key (employee_id) references employee (id)
            on update cascade on delete cascade,
    constraint sale_product_id_fk
        foreign key (product_id) references product (id)
            on update cascade on delete cascade
);

INSERT INTO positions(position)
VALUES ('Director'),
       ('Deputy Director'),
       ('Consultant'),
       ('Manager'),
       ('Seller');

INSERT INTO employee(position_id, surname, name, patronymic)
VALUES (1, 'Makisheva', 'Irina', 'Sergeevna'),
       (2, 'Romanovec', 'Evgeniy', 'Mihailovich'),
       (3, 'Rebenoc', 'Kristina', 'Igorevna'),
       (4, 'Zaharova', 'Olga', 'Ivanovna'),
       (5, 'Gavrilenko', 'Maksim', 'Alexsadrovich');

INSERT INTO client(fullName, address, phone)
VALUES ('Polyakov Gleb Rostislavovich', 'st. Hotel Passage, Building 28, Apartment 19', '8 (956) 682-53-85'),
       ('Dementieva Daria Vladislavovna', 'st. Sokolnicheskaya Ploshchad, house 82, apartment 120',
        '8 (976) 821-39-82'),
       ('Krasilnikov Valery Eduardovich', 'st. Novovorotnikovsky Lane, building 28, apartment 274',
        '8 (940) 661-17-21'),
       ('Metlushko Yana Tikhonovna', 'st. Volochaevskaya, house 28, apartment 136', '8 (967) 878-48-38'),
       ('Petrova Lilia Kirillovna', 'st. Mayakovsky Lane, building 96, apartment 18', '8 (964) 147-27-94');

INSERT INTO product(title, date, price)
VALUES ('Samsung Galaxy S10 128GB', '2019-02-20', 2358),
       ('GeForce GTX 750 Ti','2014-01-15',196),
       ('Apple MacBook Pro 13','2016-10-27',2820),
       ('SSD Kingston A400 480GB','2017-05-05',120),
       ('D-Link DIR-815/RU/R1B','2018-07-19',94);

INSERT INTO sale(dateSale, employee_id, product_id, client_id)
VALUES ('2018-06-05',5,2,3),
       ('2019-09-02',3,3,1),
       ('2019-02-13',4,1,4),
       ('2018-12-26',5,5,2),
       ('2019-07-19',3,4,5);

