package by.epam.training;

import by.epam.training.implementations.TimeService;
import by.epam.training.interfaces.ITimeService;
import by.epam.training.proxyes.MyProxy;
import by.epam.training.implementations.MetaService;
import by.epam.training.interfaces.IMetaService;

public class Run {
    public static void main(String[] args) {
        IMetaService metaService = (IMetaService) MyProxy.newInstance(new MetaService());
        metaService.printMetadataInfo();
        printHyphen();
        metaService.printMetadataExpiredTime();
        printHyphen();
        metaService.printMetadataUser();
        printHyphen();
        metaService.printMetadataBuilder();
        printHyphen();

        ITimeService timeService = (ITimeService) MyProxy.newInstance(new TimeService());
        timeService.printTimeInfo();
        printHyphen();
        timeService.printTimeExpiredTime();
        printHyphen();
        timeService.printTimeUser();
        printHyphen();
        timeService.prinTimeBuilder();
        printHyphen();
    }

    public static void printHyphen() {
        System.out.println("-------------------------");
    }
}