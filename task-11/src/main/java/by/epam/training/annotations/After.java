package by.epam.training.annotations;

import java.lang.annotation.*;

@Repeatable(After.RepeatAfter.class)
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface After {
    String message() default "Default After";

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.METHOD, ElementType.TYPE})
    @interface RepeatAfter {
        After[] value();
    }
}