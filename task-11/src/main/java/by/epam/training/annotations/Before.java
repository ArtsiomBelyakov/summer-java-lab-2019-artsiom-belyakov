package by.epam.training.annotations;

import java.lang.annotation.*;

@Repeatable(Before.RepeatBefore.class)
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Before {
    String message() default "Default Before";

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.METHOD, ElementType.TYPE})
    @interface RepeatBefore {
        Before[] value();
    }
}