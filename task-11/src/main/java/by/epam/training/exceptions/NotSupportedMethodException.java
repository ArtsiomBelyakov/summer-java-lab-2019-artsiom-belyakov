package by.epam.training.exceptions;

public class NotSupportedMethodException extends Exception {
    public NotSupportedMethodException(String message) {
        super(message);
    }
}