package by.epam.training.implementations;

import by.epam.training.annotations.*;
import by.epam.training.interfaces.IMetaService;

@After(message = "Class annotation")
public class MetaService implements IMetaService {
    @Override
    @Before(message = "Annot Before")
    @Before(message = "Test")
    public void printMetadataInfo() {
        System.out.println("printMetadataInfo + 2 Before");
    }

    @Override
    @Ignore
    @ThrowException
    public void printMetadataExpiredTime() {
        System.out.println("printMetadataExpiredTime + Ignore + ThrowException");
    }

    @Override
    @After
    @After(message = "2x")
    public void printMetadataUser() {
        System.out.println("printMetadataUser + After(2x)");
    }

    @Override
    @Before(message = "Before")
    public void printMetadataBuilder() {
        System.out.println("printMetadataBuilder + Before");
    }
}