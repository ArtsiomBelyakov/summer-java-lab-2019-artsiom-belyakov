package by.epam.training.implementations;

import by.epam.training.annotations.After;
import by.epam.training.annotations.Before;
import by.epam.training.annotations.Ignore;
import by.epam.training.annotations.ThrowException;
import by.epam.training.interfaces.ITimeService;

@Before(message = "Before class annotation")
public class TimeService implements ITimeService {

    @Before
    @After
    @Override
    public void printTimeInfo() {
        System.out.println("printTimeInfo + Before + After");
    }

    @Ignore
    @After
    @Before
    @ThrowException
    @Override
    public void printTimeExpiredTime() {
        System.out.println("printTimeExpiredTime + Ignore + After + Before + ThrowException +");
    }

    @Before
    @Override
    public void printTimeUser() {
        System.out.println("printTimeUser + Before");
    }

    @ThrowException
    @After
    @Override
    public void prinTimeBuilder() {
        System.out.println("prinTimeBuilder + ThrowException");
    }
}