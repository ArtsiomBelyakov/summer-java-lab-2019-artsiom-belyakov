package by.epam.training.interfaces;

public interface IMetaService {
    void printMetadataInfo();
    void printMetadataExpiredTime();
    void printMetadataUser();
    void printMetadataBuilder();
}