package by.epam.training.interfaces;

public interface ITimeService {
    void printTimeInfo();
    void printTimeExpiredTime();
    void printTimeUser();
    void prinTimeBuilder();
}