package by.epam.training.proxyes;

import by.epam.training.annotations.*;
import by.epam.training.exceptions.NotSupportedMethodException;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Arrays;

public class MyProxy implements InvocationHandler {
    private Object obj;

    public static Object newInstance(Object obj) {
        return java.lang.reflect.Proxy.newProxyInstance(
                obj.getClass().getClassLoader(),
                obj.getClass().getInterfaces(),
                new MyProxy(obj));
    }

    private MyProxy(Object obj) {
        this.obj = obj;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Class<?> myClass = obj.getClass();
        Method m = myClass.getMethod(method.getName(), method.getParameterTypes());

        if (m.isAnnotationPresent(Ignore.class) || myClass.isAnnotationPresent(Ignore.class)) {
            return method.invoke(obj, args);
        }

        if (m.isAnnotationPresent(ThrowException.class)) {
            throw new NotSupportedMethodException("Error");
        }

        Arrays.stream(myClass.getAnnotationsByType(Before.class)).forEach(x ->
                System.out.println(x.message()));
        Arrays.stream(m.getAnnotationsByType(Before.class)).forEach(x ->
                System.out.println(x.message()));

        method.invoke(obj, args);

        Arrays.stream(myClass.getAnnotationsByType(After.class)).forEach(x ->
                System.out.println(x.message()));

        Arrays.stream(m.getAnnotationsByType(After.class)).forEach(x ->
                System.out.println(x.message()));
        return null;
    }
}