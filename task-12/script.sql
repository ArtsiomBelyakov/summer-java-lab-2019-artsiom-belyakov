CREATE DATABASE dbusers;
USE dbusers;
create table user
(
    id    int auto_increment
        primary key,
    name varchar(56) not null,
    age int         not null
);

INSERT INTO user(name, age)
VALUES ('Valera', 24),
       ('Dasha', 23),
       ('Artem', 23);