package by.epam.training.controller;

import by.epam.training.dao.UserDAOImpl;
import by.epam.training.model.User;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @GetMapping("/{id}")
    public Object getUser(@PathVariable int id) {
        User user = new UserDAOImpl().selectById(id);
        if (user == null)
            return "Error 404";
        return user;
    }

    @PutMapping("/{id}")
    public List<User> putUser(@PathVariable int id,@RequestHeader("application/json") @RequestBody User user) {
        System.out.println(user);
        user.setId(id);
        new UserDAOImpl().update(user);
        return new UserDAOImpl().getAll();
    }

    @PostMapping
    public User postMyData(@RequestHeader("application/json") @RequestBody User user) {
        System.out.println(user);
        new UserDAOImpl().insert(user);
        return user;
    }

    @DeleteMapping("/{id}")
    public String deleteMyData(@PathVariable int id) {
        if (new UserDAOImpl().delete(id)) return "User deleted";
        return "No user";
    }
}