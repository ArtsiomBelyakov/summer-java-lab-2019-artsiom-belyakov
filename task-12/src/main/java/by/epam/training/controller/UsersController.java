package by.epam.training.controller;

import by.epam.training.dao.UserDAOImpl;
import by.epam.training.model.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UsersController {
    @GetMapping
    public List<User> getUsers() {
        return new UserDAOImpl().getAll();
    }
}