package by.epam.training.dao;

import by.epam.training.model.User;

import java.util.List;

public interface UserDAO {
    boolean insert(User user);
    List<User> getAll();
    User selectById(int id);
    boolean update(User user);
    boolean delete(int id);
}