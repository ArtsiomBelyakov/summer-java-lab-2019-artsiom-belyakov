package by.epam.training.spring.controller;

import by.epam.training.spring.model.Description;
import by.epam.training.spring.model.Product;
import by.epam.training.spring.model.User;
import by.epam.training.spring.service.ProductService;
import by.epam.training.spring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    public UserService userService;

    @Autowired
    public ProductService productService;

    @GetMapping("/product/add")
    public String addProduct() {
        return "admin/addProduct";
    }

    @PostMapping("/product/add")
    public String saveProduct(@ModelAttribute("product") Product product, Description description,
                              @RequestParam("idManufacture") int idManufacture,
                              @RequestParam("dateManufacture")
                              @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate localDate) {
        product.setDateManufacture(localDate);
        productService.save(product, description, idManufacture);
        return "redirect:/";
    }

    @GetMapping("/products")
    public String getProducts(Model model) {
        model.addAttribute("products", productService.findAll());
        return "admin/products";
    }

    @GetMapping("/product/delete/{id}")
    public String deleteProduct(@PathVariable int id) {
        productService.delete(id);
        return "redirect: /admin/products";
    }

    @GetMapping("/users")
    public String getUsers(Model model) {
        model.addAttribute("users", userService.findAll());
        return "admin/users";
    }

    @GetMapping("/user/delete/{id}")
    public String deleteUser(@PathVariable int id) {
        userService.delete(id);
        return "redirect: /admin/users";
    }

    @GetMapping("/user/add")
    public String addUser() {
        return "admin/addUser";
    }

    @PostMapping("/user/add")
    public String saveUser(@ModelAttribute("user") User user) {
        userService.save(user);
        return "redirect: /admin/users";
    }
}