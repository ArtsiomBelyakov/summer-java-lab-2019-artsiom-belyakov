package by.epam.training.spring.controller;

import by.epam.training.spring.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@PreAuthorize("isAuthenticated()")
@RequestMapping("/")
public class ProductController {

    @Autowired
    public ProductService productService;

    @GetMapping("/product/{id}")
    public String getProduct(Model model, @PathVariable int id) {
        model.addAttribute(productService.getById(id));
        return "product";
    }

    @GetMapping("/products/{id}")
    public String getProductManufacturer(Model model, @PathVariable int id) {
        model.addAttribute("products", productService.findAllByManufacturer(id));
        return "productList";
    }

    @GetMapping()
    public String getProducts(Model model) {
        model.addAttribute("products", productService.findAll());
        return "productList";
    }
}