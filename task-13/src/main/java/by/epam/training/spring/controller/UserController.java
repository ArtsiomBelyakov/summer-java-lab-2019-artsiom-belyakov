package by.epam.training.spring.controller;

import by.epam.training.spring.model.User;
import by.epam.training.spring.model.enums.UserRoleEnum;
import by.epam.training.spring.service.UserDetailsServiceImpl;
import by.epam.training.spring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/profile")
public class UserController {

    @Autowired
    public UserService userService;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @GetMapping("/{id}")
    public String getUser(@PathVariable int id, Model model) {
        model.addAttribute(userService.getById(id));
        return "profile";
    }

    @GetMapping()
    public String getUser(Model model) {
        String name = userDetailsService.getCurrentUserId();
        model.addAttribute(userService.getUser(name));
        return "profile";
    }

    @PostMapping("/{id}")
    public String putUser(@ModelAttribute("user") User user) {
        userService.update(user);
        UserRoleEnum role = userService.getUser(userDetailsService.getCurrentUserId()).getRole();
        if (role.equals(UserRoleEnum.ADMIN)) {
            userService.updateAdmin(user);
            return "redirect:/admin/users";
        }
        userService.update(user);
        return "redirect:/profile";
    }
}