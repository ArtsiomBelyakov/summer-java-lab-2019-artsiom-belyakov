package by.epam.training.spring.dao;

import by.epam.training.spring.model.Description;

public interface DescriptionDAO extends GenericDAO<Description> {
}
