package by.epam.training.spring.dao;

import java.util.List;

public interface GenericDAO<E> {
    List<E> findAll();
    void save(E entity);
    E getById(int id);
    void update(E entity);
    void delete(int id);
}