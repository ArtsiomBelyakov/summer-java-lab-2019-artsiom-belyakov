package by.epam.training.spring.dao;

import by.epam.training.spring.model.Description;
import by.epam.training.spring.model.Product;

import java.util.List;

public interface ProductDAO extends GenericDAO<Product> {
    void save(Product product, Description description, int idManufacture);
    List<Product> findAllByManufacturer(int id);
}