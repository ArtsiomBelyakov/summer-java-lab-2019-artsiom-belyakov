package by.epam.training.spring.dao;

import by.epam.training.spring.model.User;

public interface UserDAO extends GenericDAO<User> {
    User getUser(String username);
    void updateAdmin(User user);
}
