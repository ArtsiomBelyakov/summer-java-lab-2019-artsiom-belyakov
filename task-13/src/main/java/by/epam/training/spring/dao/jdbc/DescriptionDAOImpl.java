package by.epam.training.spring.dao.jdbc;

import by.epam.training.spring.dao.DescriptionDAO;
import by.epam.training.spring.mapper.DescriptionMapper;
import by.epam.training.spring.model.Description;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DescriptionDAOImpl implements DescriptionDAO {

    public final JdbcTemplate jdbcTemplate;

    @Autowired
    public DescriptionDAOImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Description> findAll() {
        return null;
    }

    @Override
    public void save(Description description) {

    }

    @Override
    public Description getById(int id) {
        String sql = "SELECT * FROM description WHERE PRODUCT_ID=?";
        return (Description) jdbcTemplate.queryForObject(sql, new DescriptionMapper(), id);
    }

    @Override
    public void update(Description description) {

    }

    @Override
    public void delete(int id) {

    }
}