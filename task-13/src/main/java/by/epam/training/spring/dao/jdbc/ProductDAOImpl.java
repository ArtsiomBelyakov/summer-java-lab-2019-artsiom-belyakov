package by.epam.training.spring.dao.jdbc;

import by.epam.training.spring.dao.ProductDAO;
import by.epam.training.spring.mapper.DescriptionMapper;
import by.epam.training.spring.mapper.ProductMapper;
import by.epam.training.spring.model.Description;
import by.epam.training.spring.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class ProductDAOImpl implements ProductDAO {
    public final JdbcTemplate jdbcTemplate;

    @Autowired
    public ProductDAOImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Product> findAll() {
        String sql = "select * from product";
        return jdbcTemplate.query(sql, new ProductMapper());
    }

    @Override
    public List<Product> findAllByManufacturer(int id) {
        String sql = "SELECT * FROM PRODUCT where manufacturer_id =?";
        return jdbcTemplate.query(sql, new ProductMapper(), id);
    }

    @Override
    public void save(Product entity) {

    }

    @Override
    @Transactional
    public void save(Product product, Description description, int idManufacture) {
        String ProductINSERT = "INSERT INTO PRODUCT(NAME, IMAGE, PRICE, dateManufacture, manufacturer_id) VALUES(?,?,?,?,?)";
        String DescriptionINSERT = "INSERT INTO DESCRIPTION(GPU_Frequency, Video_MEMORY, TYPE_VIDEO_MEMORY,MEMORY_FREQUENCY," +
                "MEMORY_BUS_WIDTH) VALUES(?,?,?,?,?)";

        jdbcTemplate.update(ProductINSERT, product.getName(), product.getImage(), product.getPrice(),
                product.getDateManufacture(), idManufacture);

        jdbcTemplate.update(DescriptionINSERT, description.getGpuFrequency(), description.getVideoMemory(),
                description.getTypeVideoMemory(), description.getMemoryFrequency(),
                description.getMemoryBusWidth());
    }

    @Override
    public Product getById(int id) {
        Product p = (Product) jdbcTemplate.queryForObject("select * from PRODUCT where ID=?", new ProductMapper(), id);
        try {
            Description d = (Description) jdbcTemplate.queryForObject("select * from DESCRIPTION where ID=?", new DescriptionMapper(), id);
            p.setDescription(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return p;
    }

    @Override
    @Transactional
    public void update(Product product) {
        String sql = "UPDATE PRODUCT SET IMAGE=?, PRICE=?, dateManufacture=? WHERE ID=?";
        jdbcTemplate.update(sql, product.getImage(), product.getPrice(),
                product.getDateManufacture(), product.getId());
    }

    @Override
    public void delete(int id) {
        String sql = "DELETE FROM PRODUCT WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }
}