package by.epam.training.spring.dao.jdbc;

import by.epam.training.spring.dao.UserDAO;
import by.epam.training.spring.mapper.UserMapper;
import by.epam.training.spring.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class UserDAOImpl implements UserDAO {

    @Autowired
    public JdbcTemplate jdbcTemplate;

    @Override
    public List findAll() {
        String sql = "select users.* ,ROLE from USERS join ROLE R on USERS.ROLE_ID = R.ROLE_ID";
        jdbcTemplate.query(sql, new UserMapper());
        return jdbcTemplate.query(sql, new UserMapper());
    }

    @Override
    @Transactional
    public void save(User user) {
        String sql = "INSERT INTO USERS(USERNAME, PASSWORD, NAME, EMAIL, PHONE, ADDRESS, ROLE_ID) VALUES(?,?,?,?,?,?,?)";
        jdbcTemplate.update(sql, user.getLogin(), user.getPassword(), user.getName(), user.getEmail(), user.getPhone(),
                user.getAddress(), user.getRoleId());
    }

    @Override
    public User getById(int id) {
        String sql = "select users.* ,ROLE from USERS join ROLE R on USERS.ROLE_ID = R.ROLE_ID  where USERS.ID=?";
        return jdbcTemplate.queryForObject(sql, new UserMapper(), id);
    }

    @Override
    @Transactional
    public void update(User user) {
        String sql = "UPDATE USERS SET NAME=?,EMAIL=?,phone=?, ADDRESS=? WHERE ID=?";
        jdbcTemplate.update(sql, user.getName(), user.getEmail(),
                user.getPhone(), user.getAddress(), user.getId());
    }

    @Override
    @Transactional
    public void delete(int id) {
        String sql = "DELETE FROM users WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }

    @Override
    public User getUser(String username) {
        String sql = "select users.* ,ROLE from USERS join ROLE R on USERS.ROLE_ID = R.ROLE_ID  where USERS.USERNAME=?";
        return jdbcTemplate.queryForObject(sql, new UserMapper(), username);
    }

    @Override
    @Transactional
    public void updateAdmin(User user) {
        String sql = "UPDATE USERS SET USERNAME=?, NAME=?,EMAIL=?,phone=?, ADDRESS=?, ROLE_ID=? WHERE ID=?";
        jdbcTemplate.update(sql, user.getLogin(), user.getName(), user.getEmail(),
                user.getPhone(), user.getAddress(), user.getRoleId(), user.getId());
    }
}