package by.epam.training.spring.interceptor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@Component
public class LogInterceptor extends HandlerInterceptorAdapter {
    private static final Logger log = LogManager.getLogger(LogInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        log.info("\n-------- LogInterception.preHandle --- ");
        log.info("User: " + request.getRemoteUser() + " " + request.getMethod() +
                " URL " + request.getRequestURL() + " @ " + getCurrentTime());
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
        log.info("\n-------- LogInterception.postHandle --- ");
        log.info("User: " + request.getRemoteUser() + " " + request.getMethod() +
                " URL " + request.getRequestURL() + " @ " + getCurrentTime());
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        log.info("\n-------- LogInterception.afterCompletion --- ");
        log.info("User: " + request.getRemoteUser() + " " + request.getMethod() +
                " URL " + request.getRequestURL() + " @ " + getCurrentTime());
    }

    private String getCurrentTime() {
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy 'at' hh:mm:ss");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        return formatter.format(calendar.getTime());
    }
}