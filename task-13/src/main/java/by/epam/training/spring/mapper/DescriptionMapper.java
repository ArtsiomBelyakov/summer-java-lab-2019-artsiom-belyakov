package by.epam.training.spring.mapper;

import by.epam.training.spring.model.Description;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DescriptionMapper implements RowMapper {
    public Description mapRow(ResultSet rs, int i) throws SQLException {
        Description description = new Description();
        description.setId(rs.getInt("id"));
        description.setGpuFrequency(rs.getString("gpu_frequency"));
        description.setVideoMemory(rs.getString("Video_memory"));
        description.setTypeVideoMemory(rs.getString("Type_video_memory"));
        description.setMemoryFrequency(rs.getString("Memory_frequency"));
        description.setMemoryBusWidth(rs.getString("Memory_bus_width"));
        return description;
    }
}