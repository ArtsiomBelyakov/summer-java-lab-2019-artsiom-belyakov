package by.epam.training.spring.mapper;

import by.epam.training.spring.model.Product;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class ProductMapper implements RowMapper {
    public Product mapRow(ResultSet rs, int i) throws SQLException {
        Product product = new Product();
        product.setId(rs.getInt("id"));
        product.setName(rs.getString("name"));
        product.setImage(rs.getString("image"));
        product.setPrice(rs.getInt("price"));
        product.setDateManufacture(LocalDate.parse(String.valueOf(rs.getDate("dateManufacture"))));
        return product;
    }
}