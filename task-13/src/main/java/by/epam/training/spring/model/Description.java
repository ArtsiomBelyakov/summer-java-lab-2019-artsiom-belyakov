package by.epam.training.spring.model;

public class Description {
    private int id;
    private String gpuFrequency;
    private String videoMemory;
    private String typeVideoMemory;
    private String memoryFrequency;
    private String memoryBusWidth;

    public Description() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGpuFrequency() {
        return gpuFrequency;
    }

    public void setGpuFrequency(String gpuFrequency) {
        this.gpuFrequency = gpuFrequency;
    }

    public String getVideoMemory() {
        return videoMemory;
    }

    public void setVideoMemory(String videoMemory) {
        this.videoMemory = videoMemory;
    }

    public String getTypeVideoMemory() {
        return typeVideoMemory;
    }

    public void setTypeVideoMemory(String typeVideoMemory) {
        this.typeVideoMemory = typeVideoMemory;
    }

    public String getMemoryFrequency() {
        return memoryFrequency;
    }

    public void setMemoryFrequency(String memoryFrequency) {
        this.memoryFrequency = memoryFrequency;
    }

    public String getMemoryBusWidth() {
        return memoryBusWidth;
    }

    public void setMemoryBusWidth(String memoryBusWidth) {
        this.memoryBusWidth = memoryBusWidth;
    }
}
