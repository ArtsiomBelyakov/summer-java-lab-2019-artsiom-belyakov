package by.epam.training.spring.model;

import java.util.List;

public class Manufacturer {
    private int id;
    private String name;
    List<Product> product;

    public Manufacturer() {
    }

    public Manufacturer(int id) {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Product> getProduct() {
        return product;
    }

    public void setProduct(List<Product> product) {
        this.product = product;
    }
}