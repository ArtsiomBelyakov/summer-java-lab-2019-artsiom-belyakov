package by.epam.training.spring.model;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

public class Product {
    private int id;
    private String name;
    private String image;
    private int price;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateManufacture;
    private Description description;

    public Product() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public LocalDate getDateManufacture() {
        return dateManufacture;
    }

    public void setDateManufacture(LocalDate dateManufacture) {
        this.dateManufacture = dateManufacture;
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }
}