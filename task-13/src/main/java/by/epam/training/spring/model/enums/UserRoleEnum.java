package by.epam.training.spring.model.enums;

public enum UserRoleEnum {
    ADMIN,
    USER,
    ANONYMOUS;

    UserRoleEnum() {}
}