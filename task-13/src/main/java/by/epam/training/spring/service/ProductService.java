package by.epam.training.spring.service;

import by.epam.training.spring.model.Description;
import by.epam.training.spring.model.Product;

import java.util.List;

public interface ProductService {
    List<Product> findAll();
    List<Product> findAllByManufacturer(int id);
    Product getById(int id);
    void save(Product product, Description description, int idManufacture);
    void delete(int id);
    Description getDescription(int id);
}