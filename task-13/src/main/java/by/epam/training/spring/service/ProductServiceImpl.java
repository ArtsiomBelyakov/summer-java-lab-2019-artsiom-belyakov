package by.epam.training.spring.service;

import by.epam.training.spring.dao.DescriptionDAO;
import by.epam.training.spring.dao.ProductDAO;
import by.epam.training.spring.model.Description;
import by.epam.training.spring.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    public ProductDAO productDAO;

    @Autowired
    public DescriptionDAO descriptionDAO;

    @Override
    public List<Product> findAll() {
        return productDAO.findAll();
    }

    @Override
    public List<Product> findAllByManufacturer(int id) {
        return productDAO.findAllByManufacturer(id);
    }

    @Override
    public Product getById(int id) {
        return productDAO.getById(id);
    }

    @Override
    public void save(Product product, Description description, int idManufacture) {
        productDAO.save(product, description, idManufacture);
    }

    @Override
    public void delete(int id) {
        productDAO.delete(id);
    }

    @Override
    public Description getDescription(int id) {
        return descriptionDAO.getById(id);
    }
}