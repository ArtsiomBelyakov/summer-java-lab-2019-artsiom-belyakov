package by.epam.training.spring.service;

import by.epam.training.spring.model.User;

import java.util.List;

public interface UserService {
    List<User> findAll();
    void save(User user);
    User getById(int id);
    void update(User user);
    void delete(int id);
    User getUser(String login);
    void updateAdmin(User user);
}