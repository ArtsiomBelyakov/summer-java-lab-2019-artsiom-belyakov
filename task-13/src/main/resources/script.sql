create table role
(
    role_id   int auto_increment
        primary key,
    role varchar(255) not null
);

INSERT INTO role(role)
VALUES ('ADMIN'),
       ('USER');

create table users
(
    id       int auto_increment
        primary key,
    username varchar(255) not null,
    password varchar(255) not null,
    name     varchar(255),
    email    varchar(255),
    phone    varchar(255),
    address  varchar(255),
    role_id  int,
    constraint users_role_id_fk
        foreign key (role_id) references role (ROLE_ID)
);


INSERT INTO users(username, password, name, email, phone, address, role_id)
VALUES ('admin', '$2y$12$gfmelR0rbE4gacnCkqcrbesRzmd.URjTvz4y2UORhBwymSbKLCRF.', 'Polyakov Gleb', 'polyakov@gmail.com', '8 (956) 682-53-85', 'Gomel', 1),
       ('user', '$2y$12$BSZj5eIDpMC7.RuBSjb0sehWwfc8pCmtqCZTAK3hWNZmlDAsYBztW', 'Dementieva Daria', 'DemDar@mail.ru', '8 (976) 821-39-82', 'Minsk', 2);
create table manufacturers
(
    manufacturer_id       int auto_increment
        primary key,
    name varchar(56) not null
);

create table product
(
    id              int auto_increment
        primary key,
    name            varchar(56) not null,
    image           varchar(128),
    price           double      not null,
    dateManufacture date,
    manufacturer_id         int,
    constraint product_typeProduct_id_fk
        foreign key (manufacturer_id) references manufacturers (manufacturer_id)
);

create table description
(
    id                int auto_increment
        primary key,
    gpu_frequency     varchar(10),
    video_Memory      varchar(10),
    type_Video_Memory varchar(10),
    memory_Frequency  varchar(10),
    memory_Bus_Width  varchar(10),
    constraint description_product_id_fk
        foreign key (id) references product (id)
            on update cascade on delete cascade
);

insert into manufacturers(name)
VALUES ('Gigabyte'),
       ('MSI'),
       ('Palit'),
       ('Sapphire'),
       ('ASUS');

INSERT INTO PRODUCT(NAME, IMAGE, PRICE,dateManufacture,manufacturer_id)
VALUES ('Sapphire Pulse RX 5700XT',
        'https://content2.onliner.by/catalog/device/header/fc8e5a0a9eea273bcf1e0c16d91186b5.jpeg', '470','2019-08-12',4),
       ('Palit GeForce GTX 1070 GameRock',
        'https://content2.onliner.by/catalog/device/header/fb42dd81b216867b48fb0f8b2c1f6952.jpeg', '360','2016-5-27',3),
       ('Gigabyte Aorus GeForce RTX 2060 Super',
        'https://content2.onliner.by/catalog/device/header/ba268cf8434e91741cc5a34a8b24197b.jpeg', '515','2019-07-23',1),
       ('MSI GeForce GTX 1060 Gaming X',
        'https://content2.onliner.by/catalog/device/header/bf413ab07255665441143f19d455ec3e.jpeg', '350','2016-07-08',2),
       ('ASUS ROG Matrix GeForce RTX 2080 Ti',
        'https://content2.onliner.by/catalog/device/header/5cc6a6a350831c9612898246009e9c6c.jpeg', '1743','2018-09-20',5);

INSERT INTO DESCRIPTION(GPU_Frequency, Video_MEMORY, TYPE_VIDEO_MEMORY, MEMORY_FREQUENCY, MEMORY_BUS_WIDTH)
VALUES ('1 670 MHz', '8 GB', 'GDDR6', '3 500 MHz', '256 bit'),
       ('1 556 MHz', '8 GB', 'GDDR5', '2 000 MHz', '256 bit'),
       ('1 470 MHz', '8 GB', 'GDDR6', '3 500 MHz', '256 bit'),
       ('1 784 MHz', '6 GB', 'GDDR5', '2 000 MHz', '192 bit'),
       ('1 800 MHz', '11 GB', 'GDDR6', '3 700 MHz', '352 bit');