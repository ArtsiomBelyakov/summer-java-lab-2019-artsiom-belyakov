<%--
  Created by IntelliJ IDEA.
  User: artim
  Date: 19.10.2019
  Time: 16:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Profile</title>
    <link href="<c:url value="/pages/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/pages/css/pricing.css" />" rel="stylesheet">

    <script src="<c:url value="/pages/js/jquery-3.3.1.slim.min.js"/>"></script>
    <script src="<c:url value="/pages/js/popper.min.js"/>"></script>
    <script src="<c:url value="/pages/js/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/pages/js/validation.js"/>"></script>
    <title>Add product</title>
</head>
<body>
<%@ include file="../header.jsp" %>
<div class="pricing-header px-3 py-3 pt-md-1 pb-md-4 mx-auto text-center">
    <h1 class="display-4">Add product</h1>
</div>

<form class="needs-validation" novalidate id="myform" method='post' action="/admin/product/add">
    <div class=" container">
        <div class="row">
            <div class="col-sm">
                <label for="validationCustom01">Manufacture</label>
                <select name="idManufacture" class="form-control">
                    <option value="1">Gigabyte</option>
                    <option value="2">MSI</option>
                    <option value="3">Palit</option>
                    <option value="4">Sapphire</option>
                    <option value="5">ASUS</option>
                </select>
                <label for="validationCustom01">Name</label>
                <input type="text" name="name" class="form-control" id="validationCustom01" placeholder="Name" required>
                <label for="validationCustomEmail">Image</label>
                <div class="input-group">
                    <input type="text" name="image" class="form-control" id="validationCustomEmail"
                           placeholder="URL image" aria-describedby="inputGroupPrepend" required>
                </div>
                <label for="validationCustom03">Price</label>
                <input type="text" name="price" class="form-control" id="validationCustom03" placeholder="Price"
                       required>
                <label for="validationCustom03">Date of manufacture</label>
                <input type="date" name="dateManufacture" class="form-control" required>
            </div>

            <div class="col-sm">
                <label>GPU frequency</label>
                <input type="text" name="gpuFrequency" class="form-control" placeholder="GPU frequency" required>
                <label>Video memory</label>
                <input type="text" name="videoMemory" class="form-control" placeholder="Video memory" required>
                <label>Type of video memory</label>
                <input type="text" name="typeVideoMemory" class="form-control" placeholder="Type of video memory"
                       required>
                <label>Memory frequency</label>
                <input type="text" name="memoryFrequency" class="form-control" placeholder="Memory frequency" required>
                <label>Memory bus width</label>
                <input type="text" name="memoryBusWidth" class="form-control" placeholder="Memory bus width" required>
            </div>
        </div>
        </p>
        <button class="btn btn-lg btn-block btn-outline-primary" type="submit">Send</button>
    </div>
</form>
</body>
</html>
