<%--
  Created by IntelliJ IDEA.
  User: artim
  Date: 20.10.2019
  Time: 15:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%--<sec:authorize access="!isAuthenticated()">--%>
<html>
<head>
    <title>Profile</title>
    <link href="<c:url value="/pages/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/pages/css/pricing.css" />" rel="stylesheet">

    <script src="<c:url value="/pages/js/jquery-3.3.1.slim.min.js"/>"></script>
    <script src="<c:url value="/pages/js/popper.min.js"/>"></script>
    <script src="<c:url value="/pages/js/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/pages/js/validation.js"/>"></script>
</head>
<body>
<%@ include file="../header.jsp" %>
<div class="pricing-header px-3 py-3 pt-md-1 pb-md-4 mx-auto text-center">
    <h1 class="display-4">Profile</h1>
</div>
<form class="needs-validation" novalidate id="myform" method='post' action="/admin/user/add">
    <div class=" container ">
        <div class="row">
            <div class="col-sm ">
                <label>Username</label>
                <input type="text" name="login" class="form-control" placeholder="Username" required>
                </p>
                <label>Password</label>
                <input type="text" name="password" class="form-control" placeholder="Password" required>
                </p>
                <label>FullName</label>
                <input type="text" name="name" class="form-control" placeholder="Full Name" required>
                </p>
                <label>Email</label>
                <input type="text" name="email" class="form-control" placeholder="email@gmail.com" required>

            </div>
            <div class="col-sm ">
                <label>Phone</label>
                <input type="text" name="phone" class="form-control" placeholder="8 (888) 888-88-88" required>
                </p>
                <label>Address</label>
                <input type="text" name="address" class="form-control" placeholder="State" required>
                <label>Role</label>
                <select name="role" class="form-control">
                    <option value="USER">USER</option>
                    <option value="ADMIN">ADMIN</option>
                </select>
            </div>
        </div>
        </p>
        <button class="btn btn-lg btn-block btn-outline-primary" type="submit">Send</button>
    </div>
</form>
</body>
</html>