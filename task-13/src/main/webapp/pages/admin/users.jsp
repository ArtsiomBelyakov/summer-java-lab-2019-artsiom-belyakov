<%--
  Created by IntelliJ IDEA.
  User: artim
  Date: 20.10.2019
  Time: 16:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <link href="<c:url value="/pages/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/pages/css/pricing.css" />" rel="stylesheet">

    <script src="<c:url value="/pages/js/jquery-3.3.1.slim.min.js"/>"></script>
    <script src="<c:url value="/pages/js/popper.min.js"/>"></script>
    <script src="<c:url value="/pages/js/bootstrap.min.js"/>"></script>
    <title>Users</title>
</head>
<body>
<%@ include file="../header.jsp" %>

<div class="pricing-header px-3 py-3 pt-md-1 pb-md-4 mx-auto text-center">
    <h1 class="display-4">Users</h1>
</div>

<div class="px-4 px-lg-0">
    <div class="pb-5">
        <div class="row">
            <div class="col-lg-12 p-5 bg-white rounded shadow-sm mb-5">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col" class="border-0 bg-light">
                                <div class="py-2 text-uppercase">Username</div>
                            </th>
                            <th scope="col" class="border-0 bg-light">
                                <div class="py-2 text-uppercase">Name</div>
                            </th>
                            <th scope="col" class="border-0 bg-light">
                                <div class="py-2 text-uppercase">Email</div>
                            </th>
                            <th scope="col" class="border-0 bg-light">
                                <div class="py-2 text-uppercase">Phone</div>
                            </th>
                            <th scope="col" class="border-0 bg-light">
                                <div class="py-2 text-uppercase">Address</div>
                            </th>
                            <th scope="col" class="border-0 bg-light">
                                <div class="py-2 text-uppercase">Role</div>
                            </th>
                            <th scope="col" class="border-0 bg-light">
                                <div class="py-2 text-uppercase">Edit</div>
                            </th>
                            <th scope="col" class="border-0 bg-light">
                                <div class="py-2 text-uppercase">Remove</div>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="user" items="${users}">
                            <tr>
                                <td class="border-0 align-middle">${user.login}</td>
                                <td class="border-0 align-middle">${user.name}</td>
                                <td class="border-0 align-middle">${user.email}</td>
                                <td class="border-0 align-middle">${user.phone}</td>
                                <td class="border-0 align-middle">${user.address}</td>
                                <td class="border-0 align-middle">${user.role}</td>
                                <td class="border-0 align-middle">
                                    <a href="${pageContext.request.contextPath}/profile/${user.id}">Edit</a>
                                </td>
                                <td class="border-0 align-middle">
                                    <a href="${pageContext.request.contextPath }/admin/user/delete/${user.id}"
                                       onclick="return confirm('Are you sure?')">Remove</a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>