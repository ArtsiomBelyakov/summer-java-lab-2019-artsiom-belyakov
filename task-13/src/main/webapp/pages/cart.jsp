<%--
  Created by IntelliJ IDEA.
  User: artim
  Date: 16.10.2019
  Time: 14:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <link href="<c:url value="/pages/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/pages/css/pricing.css" />" rel="stylesheet">

    <script src="<c:url value="/pages/js/jquery-3.3.1.slim.min.js"/>"></script>
    <script src="<c:url value="/pages/js/popper.min.js"/>"></script>
    <script src="<c:url value="/pages/js/bootstrap.min.js"/>"></script>
    <title>Cart Page</title>
</head>

<body>
<%@ include file="header.jsp" %>

<div class="pricing-header px-3 py-3 pt-md-1 pb-md-4 mx-auto text-center">
    <h1 class="display-4">Cart</h1>
</div>

<div class="px-4 px-lg-0">
    <div class="pb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 p-5 bg-white rounded shadow-sm mb-5">

                    <!-- Shopping cart table -->
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col" class="border-0 bg-light">
                                    <div class="p-2 px-3 text-uppercase">Product</div>
                                </th>
                                <th scope="col" class="border-0 bg-light">
                                    <div class="py-2 text-uppercase">Price</div>
                                </th>
                                <th scope="col" class="border-0 bg-light">
                                    <div class="py-2 text-uppercase">Quantity</div>
                                </th>
                                <th scope="col" class="border-0 bg-light">
                                    <div class="py-2 text-uppercase">Remove</div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>

                            <c:forEach var="item" items="${sessionScope.cart }">
                            <c:set var="total" value="${total + item.product.price * item.quantity }"></c:set>
                            <tr>
                                <th scope="row" class="border-0">
                                    <div class="p-2">
                                        <img src="${pageContext.request.contextPath }${item.product.image }" alt="" width="70" class="img-fluid rounded shadow-sm">
                                        <div class="ml-3 d-inline-block align-middle">
                                            <h5 class="mb-0"> <a href="/product/${item.product.id }" class="text-dark d-inline-block align-middle">${item.product.name }</a></h5>
                                            <span class="text-muted font-weight-normal font-italic d-block">Category: Graphics card</span>
                                        </div>
                                    </div>
                                </th>
                                <td class="border-0 align-middle"><strong>$${item.product.price * item.quantity}</strong></td>
                                <td class="border-0 align-middle"><strong>${item.quantity }</strong></td>
                                <td class="border-0 align-middle"><a href="${pageContext.request.contextPath }/cart/remove/${item.product.id }"
                                                                     onclick="return confirm('Are you sure?')">Remove</a></td>
                            </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <!-- End -->
                </div>
            </div>
            <small class="d-block text-left mt-3">
                <a href="${pageContext.request.contextPath }/">Continue Shopping</a>
            </small>
            <small class="d-block text-right mt-3">
                <h1 class="card-title pricing-card-title"> Sum ${total }$</h1>
            </small>
        </div>
    </div>
</div>






<%--<main role="main" class="container">--%>
<%--    <div class="my-3 p-3 bg-white rounded box-shadow">--%>
<%--        <div class="row" style="">--%>
<%--            <div class="col-sm-2"><h5>Name</h5></div>--%>
<%--            <div class="col-sm-2"><h5>Photo</h5></div>--%>
<%--            <div class="col-sm-2"><h5>Price</h5></div>--%>
<%--            <div class="col-sm-2"><h5>Quantity</h5></div>--%>
<%--            <div class="col-sm-2"><h5>Sub Total</h5></div>--%>
<%--            <div class="col-sm-2"><h5>Option</h5></div>--%>
<%--            <br>--%>

<%--            <c:forEach var="item" items="${sessionScope.cart }">--%>
<%--                <c:set var="total" value="${total + item.product.price * item.quantity }"></c:set>--%>

<%--                <div class="col-sm-2">${item.product.name }</div>--%>
<%--                <div class="col-sm-2"><img src="${pageContext.request.contextPath }${item.product.image }" width="50">--%>
<%--                </div>--%>
<%--                <div class="col-sm-2">${item.product.price }</div>--%>
<%--                <div class="col-sm-2">${item.quantity }</div>--%>
<%--                <div class="col-sm-2">${item.product.price * item.quantity }</div>--%>
<%--                <div class="col-sm-2">--%>
<%--                    <a href="${pageContext.request.contextPath }/cart/remove/${item.product.id }"--%>
<%--                       onclick="return confirm('Are you sure?')">Remove</a></div>--%>
<%--            </c:forEach>--%>
<%--        </div>--%>
<%--        <small class="d-block text-left mt-3">--%>
<%--            <a href="${pageContext.request.contextPath }/">Continue Shopping</a>--%>
<%--        </small>--%>
<%--        <small class="d-block text-right mt-3">--%>
<%--            <h1 class="card-title pricing-card-title"> Sum ${total }$</h1>--%>
<%--        </small>--%>
<%--    </div>--%>
<%--</main>--%>
</body>
</html>