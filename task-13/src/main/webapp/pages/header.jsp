<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
    <h5 class="my-0 mr-md-auto font-weight-normal"><security:authentication property="principal.username"/></h5>
    <nav class="my-2 my-md-0 mr-md-3">

        <security:authorize access="hasAuthority('ADMIN')">
            <div class="btn-group" role="group">
                <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Admin panel
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="/admin/product/add">Add product</a>
                    <a class="dropdown-item" href="/admin/users">Users</a>
                    <a class="dropdown-item" href="/admin/user/add">Add user</a>
                    <a class="dropdown-item" href="/admin/products">Products</a>
                </div>
            </div>
        </security:authorize>
        <a class="p-2 text-dark" href="/">Shop</a>
        <a class="p-2 text-dark" href="/profile">Profile</a>
        <a class="p-2 text-dark" href="/cart">Cart</a>
    </nav>
    <a class="btn btn-outline-primary" href="/logout">Logout</a>
</div>