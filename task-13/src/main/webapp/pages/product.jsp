<%--
  Created by IntelliJ IDEA.
  User: artim
  Date: 15.10.2019
  Time: 13:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%--<sec:authorize access="!isAuthenticated()">--%>
<html>
<head>
    <link href="<c:url value="/pages/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/pages/css/pricing.css" />" rel="stylesheet">

    <script src="<c:url value="/pages/js/jquery-3.3.1.slim.min.js"/>"></script>
    <script src="<c:url value="/pages/js/popper.min.js"/>"></script>
    <script src="<c:url value="/pages/js/bootstrap.min.js"/>"></script>
    <title>${product.name}</title>

</head>
<body>
<%@ include file="header.jsp" %>

<div class="container">
    <div class="card-deck mb-3 text-center align-items-end">
            <div class="card mb-4 box-shadow">
                <div class="card-header">
                    <a class="h4" role="button">${product.name}</a></p>
                </div>
                <div class="card-body"><img src="${pageContext.request.contextPath }${product.image }" style="">
                    <h1 class="card-title pricing-card-title"> ${product.price}$ </h1>
                    <ul class="list-unstyled mt-3 mb-4">
                        <c:if test="${empty product.description}">
                            <li>Description coming soon</li>
                        </c:if>
                        <c:if test="${not empty product.description}">
                            <li><b>GPU frequency: </b> ${product.description.gpuFrequency}</li>
                            <li><b>Video memory: </b> ${product.description.videoMemory}</li>
                            <li><b>Type of video memory: </b> ${product.description.typeVideoMemory}</li>
                            <li><b>Memory frequency: </b> ${product.description.memoryFrequency}</li>
                            <li><b>Memory bus width: </b> ${product.description.memoryBusWidth}</li>
                        </c:if>
                    </ul>
                    <a class="btn btn-lg btn-block btn-outline-primary" href="/cart/buy/${product.id}"
                       role="button">Buy</a></p>
                </div>
            </div>
    </div>
</div>
</body>
</html>