<%--
  Created by IntelliJ IDEA.
  User: artim
  Date: 15.10.2019
  Time: 12:35
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <link href="<c:url value="/pages/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/pages/css/pricing.css" />" rel="stylesheet">

    <script src="<c:url value="/pages/js/jquery-3.3.1.slim.min.js"/>"></script>
    <script src="<c:url value="/pages/js/popper.min.js"/>"></script>
    <script src="<c:url value="/pages/js/bootstrap.min.js"/>"></script>
    <title>Shop</title>
</head>
<body>
<%@ include file="header.jsp" %>

<div class="pricing-header px-3 py-3 pt-md-1 pb-md-4 mx-auto text-center">
    <h1 class="display-4">Shop</h1>
</div>

<div class="row">
    <div class="col-lg-8 mx-auto">
        <div class="nav-scroller py-1 mb-2">
            <nav class="nav d-flex justify-content-between">
                <a class="h4" href="/products/1" role="button">Gigabyte</a>
                <a class="h4" href="/products/2" role="button">MSI</a>
                <a class="h4" href="/products/3" role="button">Palit</a>
                <a class="h4" href="/products/4" role="button">Sapphire</a>
                <a class="h4" href="/products/5" role="button">ASUS</a>
            </nav>
        </div>
        <!-- List group-->
        <ul class="list-group shadow">
            <!-- list group item-->
            <c:forEach var="product" items="${products}">
                <li class="list-group-item">
                    <!-- Custom content-->
                    <div class="media align-items-lg-center flex-column flex-lg-row p-3">
                        <div class="media-body order-2 order-lg-1">
                            <h5 class="mt-0 font-weight-bold mb-2"><a class="h4" href="/product/${product.id}"
                                                                      role="button">${product.name}</a></h5>
                            <p class="font-italic text-muted mb-0 small">${product.dateManufacture}</p>To view details, click in a uber nomen.</p>
                            <div class="d-flex align-items-center justify-content-between mt-1">
                                <h6 class="font-weight-bold my-2">$${product.price}</h6>
                            </div>
                            </p><a class="btn btn-lg btn-block btn-outline-primary" href="/cart/buy/${product.id}"
                                   role="button">Buy</a></p>
                        </div>
                        <img src="${pageContext.request.contextPath }${product.image }" alt="Generic placeholder image"
                             width="200" class="ml-lg-5 order-1 order-lg-2">
                    </div>
                    <!-- End -->
                </li>
            </c:forEach>
            <!-- End -->
        </ul>
        <!-- End -->
    </div>
</div>
</body>
</html>