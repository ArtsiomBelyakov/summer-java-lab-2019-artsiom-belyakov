<%--
  Created by IntelliJ IDEA.
  User: artim
  Date: 17.10.2019
  Time: 21:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%--<sec:authorize access="!isAuthenticated()">--%>
<html>
<head>
    <title>Profile</title>
    <link href="<c:url value="/pages/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/pages/css/pricing.css" />" rel="stylesheet">

    <script src="<c:url value="/pages/js/jquery-3.3.1.slim.min.js"/>"></script>
    <script src="<c:url value="/pages/js/popper.min.js"/>"></script>
    <script src="<c:url value="/pages/js/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/pages/js/validation.js"/>"></script>
</head>
<body>
<%@ include file="header.jsp" %>
<div class="pricing-header px-3 py-3 pt-md-1 pb-md-4 mx-auto text-center">
    <h1 class="display-4">Profile</h1>
</div>
<form class="needs-validation" novalidate id="myform" method='post' action="/profile/${user.id}">
    <div class=" container ">
        <div class="row">
            <div class="col-sm ">
                <security:authorize access="hasAuthority('ADMIN')">
                    <label>Username</label>
                    <input type="text" name="login" class="form-control" placeholder="Username" value="${user.login}"
                           required>
                    </p>
                </security:authorize>
                <label>FullName</label>
                <input type="text" name="name" class="form-control" placeholder="Full Name" value="${user.name}"
                       required>
                </p>
                <label>Email</label>
                <input type="text" name="email" class="form-control" placeholder="email@gmail.com" value="${user.email}"
                       required>

            </div>
            <div class="col-sm ">
                <label>Phone</label>
                <input type="text" name="phone" class="form-control" placeholder="8 (888) 888-88-88"
                       value="${user.phone}" required>
                </p>
                <label>Address</label>
                <input type="text" name="address" class="form-control" placeholder="State" value="${user.address}"
                       required>
                <security:authorize access="hasAuthority('ADMIN')">
                </p>
                <label>Role</label>
                <select name="role" id="role" class="form-control">
                    <script>
                        if ('${user.role}' == "ADMIN") {
                            document.getElementById('role').innerHTML = '<option value="ADMIN">ADMIN</option><option value="USER">USER</option>';
                        } else {
                            document.getElementById('role').innerHTML = '<option value="USER">USER</option><option value="ADMIN">ADMIN</option>';
                        }
                    </script>
                </select>
                </security:authorize >
            </div>
        </div>
        </p>
        <button class="btn btn-lg btn-block btn-outline-primary" type="submit">Update</button>
    </div>
</form>
</body>
</html>