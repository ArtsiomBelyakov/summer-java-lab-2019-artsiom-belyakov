import builder.Client;
import decorator.DoubleExtra;
import decorator.Menu;
import decorator.Order;
import decorator.RegularExtra;
import history.History;

import java.util.LinkedHashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        Client client = Client.newBuilder()
                .setName("Test")
                .setCity("Rechitsa")
                .setAge(18)
                .setEmail("art@gmail.com")
                .build();
        System.out.println(client);
        System.out.println();

        Map<Integer, Menu> priceMenu = new LinkedHashMap<>();
        priceMenu.put(1, new Menu("Basis for pizza", 3.50));
        priceMenu.put(2, new Menu("Thin pita bread", 0.9));
        priceMenu.put(3, new Menu("Smoked sausage", 2.50));
        priceMenu.put(4, new Menu("Jerked sausage", 2.0));
        priceMenu.put(5, new Menu("Ham", 2.0));
        priceMenu.put(6, new Menu("Sausages", 1.50));
        priceMenu.put(7, new Menu("Fowl", 1.50));
        priceMenu.put(8, new Menu("Mussel Meat", 2.50));
        priceMenu.put(9, new Menu("Squid Meat", 2.50));
        priceMenu.put(10, new Menu("Shrimp meat", 2.50));
        priceMenu.put(11, new Menu("Crab Sticks", 3.00));
        priceMenu.put(12, new Menu("Lamb", 2.50));
        priceMenu.put(13, new Menu("Pepper sweet", 1.50));
        priceMenu.put(14, new Menu("Fresh Tomato", 1.00));
        priceMenu.put(15, new Menu("Mushrooms", 1.00));
        priceMenu.put(16, new Menu("Karnishons", 1.00));
        priceMenu.put(17, new Menu("Sweet corn", 1.00));
        priceMenu.put(18, new Menu("Olives", 1.00));
        priceMenu.put(19, new Menu("Olive-tree", 1.00));
        priceMenu.put(20, new Menu("Pickled Cucumber", 0.9));
        priceMenu.put(21, new Menu("Ketchup", 0.9));
        priceMenu.put(22, new Menu("Potatoes", 0.9));
        priceMenu.put(23, new Menu("Korean Carrots", 0.9));
        priceMenu.put(24, new Menu("Chicken Fillet", 0.9));
        priceMenu.put(25, new Menu("Armenian thin pita bread", 0.9));
        priceMenu.put(26, new Menu("Greens", 0.9));
        printIngredients(priceMenu);

        Order order = priceMenu.get(2);
        order = new RegularExtra(priceMenu.get(5), order);

        History history = new History();
        history.setMemento(order.saveState());

        System.out.println(order.getLabel());
        System.out.println(order.getPrice());

        order = new RegularExtra(priceMenu.get(15), order);
        order = new DoubleExtra(priceMenu.get(19), order);
        order = new RegularExtra(priceMenu.get(14), order);

        System.out.println(order.getLabel());
        System.out.println(order.getPrice());

        order.restoreState(history.getMemento());
        System.out.println(order.getLabel());
        System.out.println(order.getPrice());
        order = new DoubleExtra(priceMenu.get(10), order);
        System.out.println(order.getLabel());
        System.out.println(order.getPrice());

    }

    private static void printIngredients(Map<Integer, Menu> ingredientMap) {
        System.out.println("Available Ingredients:");
        for (Map.Entry<Integer, Menu> entry : ingredientMap.entrySet()) {
            System.out.println(entry.getKey() + ": "
                    + entry.getValue());
        }
    }
}
