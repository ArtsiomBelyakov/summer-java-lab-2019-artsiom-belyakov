package builder;

public class Client {

    private String name;
    private String surname;
    private int age;
    private String phone;
    private String city;
    private String email;

    public static Builder newBuilder() {
        return new Client().new Builder();
    }

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", phone='" + phone + '\'' +
                ", city='" + city + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public class Builder {

        private Builder() {
        }

        public Builder setName(String name) {
            Client.this.name = name;
            return this;
        }

        public Builder setSurname(String surname) {
            Client.this.surname = surname;
            return this;
        }

        public Builder setAge(int age) {
            Client.this.age = age;
            return this;
        }

        public Builder setPhone(String phone) {
            Client.this.phone = phone;
            return this;
        }

        public Builder setCity(String city) {
            Client.this.city = city;
            return this;
        }

        public Builder setEmail(String email) {
            Client.this.email = email;
            return this;
        }

        public Client build() {
            return Client.this;
        }
    }
}
