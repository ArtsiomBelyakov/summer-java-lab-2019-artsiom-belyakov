package decorator;

public class DoubleExtra extends Extra {

    public DoubleExtra(String label, double price, Order order) {
        super(label, price, order);
    }

    public DoubleExtra(Menu menu, Order order) {
        super(menu, order);
    }

    @Override
    public double getPrice() {
        return (this.price * 2) + order.getPrice();
    }

    @Override
    public String getLabel() {
        return order.getLabel() + ", Double " + this.label;
    }


}