package decorator;

import history.Memento;

public abstract class Extra implements Order {

    protected String label;
    protected double price;
    protected Order order;

    public Extra(Menu menu, Order order) {
        this.label = menu.getLabel();
        this.price = menu.getPrice();
        this.order = order;
    }

    public Extra(String label, double price, Order order) {
        this.label = label;
        this.price = price;
        this.order = order;
    }

    public abstract double getPrice();

    @Override
    public String getLabel() {
        return order.getLabel() + ", " + this.label;
    }

    @Override
    public Memento saveState() {
        return new Memento(label, price, order);
    }

    @Override
    public void restoreState(Memento memento) {
        this.label = memento.getLabel();
        this.price = memento.getPrice();
        this.order = memento.getOrder();
    }
}
