package decorator;

import history.Memento;

public class Menu implements Order {

    private String label;
    private double price;

    public Menu(String label, double price) {

        this.label = label;
        this.price = price;
    }

    public double getPrice() {
        return Math.round(this.price * 100.0) / 100.0;
    }

    public String getLabel() {
        return this.label;
    }

    public Memento saveState() {
        return new Memento(label, price);
    }

    public void restoreState(Memento memento) {
        this.label = memento.getLabel();
        this.price = memento.getPrice();
    }

    @Override
    public String toString() {
        return label + ", Price=" + price;
    }
}

