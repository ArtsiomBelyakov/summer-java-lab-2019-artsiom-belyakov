package decorator;

import history.Memento;

public interface Order {
    double getPrice();

    String getLabel();

    Memento saveState();

    void restoreState(Memento memento);
}