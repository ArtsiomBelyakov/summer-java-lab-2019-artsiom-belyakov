package decorator;

public class RegularExtra extends Extra {

    public RegularExtra(String label, double price, Order order) {
        super(label, price, order);
    }

    public RegularExtra(Menu menu, Order order) {
        super(menu, order);
    }

    @Override
    public double getPrice() {
        return this.price + order.getPrice();
    }
}