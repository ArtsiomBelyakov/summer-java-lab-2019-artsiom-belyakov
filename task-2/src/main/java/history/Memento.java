package history;

import decorator.Order;

public class Memento {

    private String label;
    private double price;
    private Order order;

    public Memento(String label, double price) {
        this.label = label;
        this.price = price;
    }

    public Memento(String label, double price, Order order) {
        this.label = label;
        this.price = price;
        this.order = order;
    }

    public String getLabel() {
        return label;
    }

    public double getPrice() {
        return price;
    }

    public Order getOrder() {
        return order;
    }
}
