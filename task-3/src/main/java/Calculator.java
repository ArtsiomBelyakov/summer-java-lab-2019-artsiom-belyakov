import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

class Calculator {
    private static List<String> oper;

    public static void main(String[] args) {
        List<String> stringList = getFile(args[0]);
        List<Expr> resultList = new ArrayList<>();
        for (String str : stringList) {
            oper = new ArrayList<>();
            int index = (stringList.indexOf(str)) + 1;
            resultList.add(new Expr(index, oper, calc(str), str.replaceAll("\\s+", "")));
        }
        for (Expr v : resultList) {
            if (v.getResult().equals("Incorrect expression") || v.getResult().equals("Infinity"))
                System.out.printf("%d.  %s%n", v.getId(), v.getResult());
            else
                System.out.println(v.toString());
        }
        getNumber(resultList);
    }

    private static List<String> getFile(String arg) {
        Path filePath = new File(arg).toPath();
        Charset charset = Charset.defaultCharset();
        List<String> stringList = null;
        try {
            stringList = Files.readAllLines(filePath, charset);
        } catch (IOException e) {
            System.out.println("Incorrect path");
            System.exit(0);
        }
        return stringList;
    }

    private static boolean correctBrackets(String s) {
        int counter = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(')
                counter++;
            else if (s.charAt(i) == ')') counter--;
            if (counter < 0)
                return false;
        }
        return counter == 0;
    }

    private static void processOperator(Stack<Double> st, String op) {
        double last = st.pop(), prev = st.pop();
        switch (op) {
            case "+":
                st.push(last + prev);
                oper.add("" + last + "+" + prev);
                break;
            case "-":
                st.push(prev - last);
                oper.add("" + prev + "-" + last);
                break;
            case "*":
                st.push(last * prev);
                oper.add("" + last + "*" + prev);
                break;
            case "/":
                st.push(prev / last);
                oper.add("" + prev + "/" + last);
                break;
        }
    }

    private static boolean isOperator(String c) {
        return c.equals("+") || c.equals("-") || c.equals("*") || c.equals("/") || c.equals("%");
    }

    private static int priority(String op) {
        switch (op) {
            case "+":
            case "-":
                return 1;
            case "*":
            case "/":
                return 2;
            default:
                return -1;
        }
    }

    private static String calc(String s) {
        Stack<Double> st = new Stack<>(); //stack of numbers
        Stack<String> oper = new Stack<>(); //stack of operations
        if (!correctBrackets(s) || s.matches("(.*)[A-z](.*)"))
            return "Incorrect expression";
        s = s.replaceAll("\\s+", ""); //remove spaces
        s = s.replaceAll("\\d*[.,\\d]\\d|.(?!$)", "$0 "); //add spaces
        String[] arrStr = s.split(" ");
        for (String anArrStr : arrStr) {
            if (anArrStr.equals("("))
                oper.push("(");
            else if (anArrStr.equals(")")) {
                while (!oper.peek().equals("("))
                    processOperator(st, oper.pop());
                oper.pop();
            } else if (isOperator(anArrStr)) {
                while (!oper.isEmpty() && priority(oper.peek()) >= priority(anArrStr))
                    processOperator(st, oper.pop());
                oper.add(anArrStr);
            } else {
                String operand = "";
                operand += anArrStr;
                st.push(Double.parseDouble(operand));
            }
        }
        while (!oper.isEmpty())
            processOperator(st, oper.pop());
        return st.peek().toString();
    }

    private static void getNumber(List<Expr> resultList) {
        System.out.print("Enter number and step: \n");
        int num, step;
        try (Scanner in = new Scanner(System.in)) {
            num = in.nextInt();
            step = in.nextInt();
            try {
                System.out.println(resultList.get(num - 1).getNumber().get(step - 1));
            } catch (IndexOutOfBoundsException e) {
                System.out.println(resultList.get(num - 1).getId() + ". Incorrect value");
            }
        } catch (InputMismatchException e) {
            System.out.println("Incorrect value");
        }
    }
}