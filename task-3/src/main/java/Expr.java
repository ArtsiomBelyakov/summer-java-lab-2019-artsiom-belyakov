import java.util.List;

public class Expr {
    private long id;
    private List<String> number;
    private String result;
    private String expression;

    public Expr(long id, List<String> number, String result, String expression) {
        this.id = id;
        this.number = number;
        this.result = result;
        this.expression = expression;
    }

    public long getId() {
        return id;
    }

    public String getResult() {
        return result;
    }

    public List<String> getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return id + ".  " +
                number.size() + " steps; " +
                result + " result; " +
                expression;
    }
}