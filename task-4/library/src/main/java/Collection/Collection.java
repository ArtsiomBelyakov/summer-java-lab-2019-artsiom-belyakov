package Collection;

import Iterator.Iterator;

public interface Collection<E> {
    Iterator getIterator();
    int size();
    <T> T[] toArray(T[] a);
}