package Entity;

public interface Entity<K,V> {
    K getKey();
    V getValue();
}
