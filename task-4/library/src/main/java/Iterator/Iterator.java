package Iterator;

import java.util.Objects;
import java.util.function.Consumer;

public interface Iterator<E> {
    E getNext();
    boolean hasNext();
    void remove();
    void addBefore(E e);
    void addAfter(E e);
    default void forEachRemaining(Consumer<? super E> action) {
        Objects.requireNonNull(action);
        while (hasNext())
            action.accept(getNext());
    }
}