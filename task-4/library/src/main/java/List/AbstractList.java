package List;

public abstract class AbstractList<E> implements List<E> {

    @Override
    public E getNext() {
        return null;
    }

    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public void remove() {

    }

    @Override
    public void addBefore(E e) {

    }

    @Override
    public void addAfter(E e) {

    }
}