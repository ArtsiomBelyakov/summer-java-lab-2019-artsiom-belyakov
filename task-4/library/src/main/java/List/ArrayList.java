package List;

import Collection.Collection;
import Iterator.Iterator;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;


public class ArrayList<E> extends AbstractList<E> implements List<E> {
    private static final int DEFAULT_CAPACITY = 10;
    private static final int CUT_RATE = 4;
    private int MAX_ARRAY_SIZE;
    private E[] elements;
    private int size;

    public ArrayList() {
        elements = (E[]) new Object[DEFAULT_CAPACITY];
    }

    @Override
    public int add(E value) {
        if (MAX_ARRAY_SIZE == 0) {
            if (size == elements.length - 1)
                resize((elements.length * 3) / 2 + 1);
            elements[size++] = value;
        } else if (size < elements.length)
            elements[size++] = value;
        sort();
        return find(value);
    }

    public void sort() {
        trim();
        Arrays.sort((E[]) elements, 0, size);
    }

    @Override
    public void addAll(Collection<? extends E> c) {
        Object[] a = c.toArray(new Object[c.size()]);
        addAll((E[]) a);
    }

    @Override
    public void addAll(E[] a) {
        int numNew;
        if (MAX_ARRAY_SIZE == 0) {
            numNew = a.length;
            resize(size + numNew);
        } else {
            numNew = MAX_ARRAY_SIZE - size;
        }
        System.arraycopy(a, 0, elements, size, numNew);
        size += numNew;
    }

    @Override
    public E set(int index, E element) {
        if (index >= size)
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        E oldValue = elements[index];
        elements[index] = element;
        sort();
        return oldValue;
    }

    public E remove(int index) {
        E element = elements[index];
        for (int i = index; i < size - 1; i++)
            elements[i] = elements[i + 1];
        elements[size - 1] = null;
        size--;
        if (elements.length > DEFAULT_CAPACITY && size < elements.length / CUT_RATE)
            resize(elements.length / 2);
        return element;
    }

    @Override
    public void clear() {
        for (int i = 0; i < size; i++)
            elements[i] = null;
        size = 0;
    }

    @Override
    public int find(E element) {
        if (element == null) {
            for (int i = 0; i < size; i++) {
                if (elements[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (element.equals(elements[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    public E get(int index) {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size " + size);
        }
        return elements[index];
    }

    public <T> T[] toArray(T[] a) {
        if (a.length < size)
            return (T[]) Arrays.copyOf(elements, size, a.getClass());
        System.arraycopy(elements, 0, a, 0, size);
        if (a.length > size)
            a[size] = null;
        return a;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void trim() {
        for (int i = 0; i < size; i++) {
            if (elements[i] == null) {
                remove(i);
                i--;
            }
        }
    }

    private void filterMatches() {
        Arrays.sort(elements);
        for (int i = 1; i < size; i++) {
            if (elements[i - 1].equals(elements[i])) {
                elements[i - 1] = null;
                elements[i] = null;
                trim();
                i--;
            }
        }
    }

    @Override
    public void filterMatches(Collection<? extends E> c) {
        addAll(c);
        filterMatches();
    }

    @Override
    public void filterMatches(E[] a) {
        addAll(a);
        filterMatches();
    }

    @Override
    public void filterDifference(Collection<? extends E> c) {
        Object[] array = new Object[c.size()];
        array = c.toArray(array);
        filterDifference((E[]) array);
    }

    @Override
    public void filterDifference(E[] a) {
        E[] newArray = (E[]) new Object[size];
        System.arraycopy(elements, 0, newArray, 0, size);

        if (MAX_ARRAY_SIZE == 0)
            elements = (E[]) new Object[DEFAULT_CAPACITY];
        else elements = (E[]) new Object[MAX_ARRAY_SIZE];
        size = 0;
        for (E aNewArray : newArray) {
            for (int j = 0; j < a.length; j++) {
                if (aNewArray.equals(a[j])) {
                    elements[size++] = aNewArray;
                }
            }
        }
    }

    private void resize(int newLength) {
        E[] newArray = (E[]) new Object[newLength];
        System.arraycopy(elements, 0, newArray, 0, size);
        elements = newArray;
    }

    public void setMaxSize(int maxCapacity) {
        MAX_ARRAY_SIZE = maxCapacity;
        if (size == 0) {
            elements = (E[]) new Object[MAX_ARRAY_SIZE];
        } else {
            E[] newArray = (E[]) new Object[MAX_ARRAY_SIZE];
            System.arraycopy(elements, 0, newArray, 0, MAX_ARRAY_SIZE);
            elements = newArray;
            //size = size - MAX_ARRAY_SIZE;
            size = elements.length;
        }
    }

    @Override
    public int getMaxSize(int maxSize) {
        return MAX_ARRAY_SIZE;
    }

    @Override
    public Iterator<E> getIterator() {
        return new Itr();
    }

    private class Itr implements Iterator<E> {
        int cursor;       // index of next element to return
        int lastRet = -1; // index of last element returned; -1 if no such

        Itr() {
        }

        public boolean hasNext() {
            return cursor != size;
        }

        @Override
        public E getNext() {
            int i = cursor;
            if (i >= size)
                throw new NoSuchElementException();
            Object[] elementData = ArrayList.this.elements;
            if (i >= elementData.length)
                throw new ConcurrentModificationException();
            cursor = i + 1;
            return (E) elementData[lastRet = i];
        }

        public void remove() {
            if (lastRet < 0)
                throw new IllegalStateException();

            try {
                ArrayList.this.remove(lastRet);
                cursor = lastRet;
                lastRet = -1;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        @Override
        public void addBefore(E e) {
            for (int i = size; i >= lastRet; i--) {
                elements[i + 1] = elements[i];
                elements[i] = e;
            }
            size++;
        }

        @Override
        public void addAfter(E e) {
            for (int i = size; i - 1 >= lastRet; i--) {
                elements[i + 1] = elements[i];
                elements[i] = e;
            }
            size++;
        }
    }
}