package List;

import Collection.Collection;
import Iterator.Iterator;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class LinkedList<E> extends AbstractList<E> implements List<E> {
    private Node<E> first;
    private Node<E> last;
    private int size = 0;
    private int MAX_ARRAY_SIZE;

    @Override
    public Iterator getIterator() {
        return new ListItr(0);
    }

    @Override
    public int add(E element) {
        if (MAX_ARRAY_SIZE == 0) {
            linkLast(element);
            sort();
        } else if (size < MAX_ARRAY_SIZE) {
            linkLast(element);
            sort();
        }
        return find(element);
    }

    private void linkLast(E e) {
        final Node<E> l = last;
        final Node<E> newNode = new Node<>(l, e, null);
        last = newNode;
        if (l == null)
            first = newNode;
        else
            l.next = newNode;
        size++;
    }

    private Node<E> node(int index) {
        if (index < (size >> 1)) {
            Node<E> x = first;
            for (int i = 0; i < index; i++)
                x = x.next;
            return x;
        } else {
            Node<E> x = last;
            for (int i = size - 1; i > index; i--)
                x = x.prev;
            return x;
        }
    }

    @Override
    public void addAll(Collection c) {
        addAll(size, c);
    }

    private boolean addAll(int index, Collection<? extends E> c) {
        Object[] a = c.toArray(new Object[c.size()]);
        int numNew = a.length;
        if (numNew == 0)
            return false;
        Node<E> pred, succ;
        if (index == size) {
            succ = null;
            pred = last;
        } else {
            succ = node(index);
            pred = succ.prev;
        }
        for (Object o : a) {
            @SuppressWarnings("unchecked") E e = (E) o;
            Node<E> newNode = new Node<>(pred, e, null);
            if (pred == null)
                first = newNode;
            else
                pred.next = newNode;
            pred = newNode;
        }
        if (succ == null) {
            last = pred;
        } else {
            pred.next = succ;
            succ.prev = pred;
        }
        size += numNew;
        return true;
    }

    @Override
    public void addAll(Object[] a) {
        for (Object anA : a) {
            linkLast((E) anA);
        }
    }

    @Override
    public E set(int index, Object element) {
        if (!(index >= 0 && index < size))
            throw new IndexOutOfBoundsException("Index: " + index + ", Size " + size);
        Node<E> x = node(index);
        E oldVal = x.item;
        x.item = (E) element;
        sort();
        return oldVal;
    }

    @Override
    public E remove(int index) {
        Node<E> x = node(index);
        final E element = x.item;
        final Node<E> next = x.next;
        final Node<E> prev = x.prev;
        if (prev == null) {
            first = next;
        } else {
            prev.next = next;
            x.prev = null;
        }

        if (next == null) {
            last = prev;
        } else {
            next.prev = prev;
            x.next = null;
        }
        x.item = null;
        size--;
        return element;
    }

    @Override
    public void clear() {
        for (Node<E> x = first; x != null; ) {
            Node<E> next = x.next;
            x.item = null;
            x.next = null;
            x.prev = null;
            x = next;
        }
        first = last = null;
        size = 0;
    }

    @Override
    public int find(E element) {
        Node<E> elem = new Node<>(null, null, null);
        elem.item = element;
        for (int i = 0; i < size; i++)
            if (node(i).item.equals(elem.item))
                return i;
        return -1;
    }

    void sort() {
        E[] a = this.toArray((E[]) new Object[size]);
        Arrays.sort(a, 0, size);
        resetNode();
        for (E anA : a) {
            linkLast(anA);
        }
    }

    private void resetNode() {
        first = null;
        last = null;
        size = 0;
    }

    @Override
    public E get(int index) {
        if (!(index >= 0 && index < size)) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size " + size);
        }
        return node(index).item;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void trim() {
        for (int i = 0; i < size; i++) {
            if (node(i).item == null) {
                remove(i);
                i--;
            }
        }
    }

    private void filterMatches() {
        sort();
        for (int i = 1; i < size; i++) {
            if (node(i - 1).item.equals(node(i).item)) {
                remove(i);
                remove(i - 1);
            }
        }
    }

    @Override
    public void filterMatches(Collection c) {
        addAll(c);
        filterMatches();
    }

    @Override
    public void filterMatches(Object[] a) {
        addAll(a);
        filterMatches();
    }

    @Override
    public void filterDifference(Collection c) {
        Object[] array = new Object[c.size()];
        array = c.toArray(array);
        filterDifference(array);
    }

    @Override
    public void filterDifference(Object[] a) {
        E[] newArray = toArray((E[]) new Object[size]);
        clear();
        for (E aNewArray : newArray) {
            for (int j = 0; j < a.length; j++) {
                if (aNewArray.equals(a[j])) {
                    add(aNewArray);
                }
            }
        }
    }

    @Override
    public void setMaxSize(int maxSize) {
        MAX_ARRAY_SIZE = maxSize;
        if (size != 0) {
            E[] a = this.toArray((E[]) new Object[size]);
            resetNode();
            for (int i = 0; i < MAX_ARRAY_SIZE; i++) {
                linkLast(a[i]);
            }
        }
    }

    @Override
    public int getMaxSize(int maxSize) {
        return MAX_ARRAY_SIZE;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        Object[] result = new Object[size];
        int i = 0;
        for (Node<E> x = first; x != null; x = x.next)
            result[i++] = x.item;
        return (T[]) result;
    }

    private void linkFirst(E e) {
        final Node<E> f = first;
        final Node<E> newNode = new Node<>(null, e, f);
        first = newNode;
        if (f == null)
            last = newNode;
        else
            f.prev = newNode;
        size++;
    }

    private E unlink(Node<E> x) {
        final E element = x.item;
        final Node<E> next = x.next;
        final Node<E> prev = x.prev;
        if (prev == null) {
            first = next;
        } else {
            prev.next = next;
            x.prev = null;
        }
        if (next == null) {
            last = prev;
        } else {
            next.prev = prev;
            x.next = null;
        }
        x.item = null;
        size--;
        return element;
    }

    private static class Node<E> {
        E item;
        Node<E> next;
        Node<E> prev;

        Node(Node<E> prev, E element, Node<E> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }
    }

    private class ListItr implements Iterator<E> {
        private Node<E> lastReturned;
        private Node<E> next;
        private int nextIndex;

        ListItr(int index) {
            next = (index == size) ? null : node(index);
            nextIndex = index;
        }

        public boolean hasNext() {
            return nextIndex < size;
        }

        public E getNext() {
            if (!hasNext())
                throw new NoSuchElementException();

            lastReturned = next;
            next = next.next;
            nextIndex++;
            return lastReturned.item;
        }

        public void remove() {
            if (lastReturned == null)
                throw new IllegalStateException();

            Node<E> lastNext = lastReturned.next;
            unlink(lastReturned);
            if (next == lastReturned)
                next = lastNext;
            else
                nextIndex--;
            lastReturned = null;
        }

        @Override
        public void addBefore(E e) {
            lastReturned = null;
            if (next == null)
                linkLast(e);
            else {
                final Node<E> pred = next.prev;
                final Node<E> newNode = new Node<>(pred, e, next);
                next.prev = newNode;
                if (pred == null)
                    first = newNode;
                else
                    pred.next = newNode;
                size++;
            }
            nextIndex++;
        }

        @Override
        public void addAfter(E e) {
            if (lastReturned.prev == null)
                linkFirst(e);
            else {
                final Node<E> pred = lastReturned.prev;
                final Node<E> newNode = new Node<>(pred, e, lastReturned);
                lastReturned.prev = newNode;
                if (pred == null)
                    first = newNode;
                else
                    pred.next = newNode;
                size++;
            }
            nextIndex++;
        }
    }
}