package List;

import Collection.Collection;
import Iterator.Iterator;

public interface List<E> extends Collection<E>,Iterator<E>{
    Iterator<E> getIterator();
    int add(E element);
    void addAll(Collection<? extends E> c);
    void addAll(E[] a);
    E set(int index, E element);
    E remove(int index);
    void clear();
    int find(E element);
    E get(int index);
    <T> T[] toArray(T[] a);
    int size();
    void trim();
    void filterMatches(Collection<? extends E> c);
    void filterMatches(E[] a);
    void filterDifference(Collection<? extends E> c);
    void filterDifference(E[] a);
    void setMaxSize(int maxSize);
    int getMaxSize(int maxSize);
}