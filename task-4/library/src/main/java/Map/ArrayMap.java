package Map;

import Entity.*;
import List.*;
import Map.*;

import java.util.Arrays;
import java.util.Objects;

public class ArrayMap<K, V> implements Map<K, V> {
    static final int DEFAULT_CAPACITY = 16;
    protected int size = 0;
    protected Node<K, V>[] table;

    public ArrayMap() {
        this(DEFAULT_CAPACITY);
    }

    public ArrayMap(int capacity) {
        this.table = new Node[capacity];
    }

    protected static int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

    protected int getIndex(int hash) {
        return hash & (table.length - 1);
    }

    private int getTableSize() {
        return table.length;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public void set(K key, V value) throws Exception {
        int hash = hash(key);
        int index = getIndex(hash);
        Node<K, V> e = table[index];
        Node<K, V> p;
        Node<K, V> node = null;
        K k;
        if (e == null) {
            table[index] = new Node<>(hash(key), key, value);
            size++;
        } else if (e.hash == hash && (e.getKey() == key || key.equals(e.getKey()))) {
            throw new Exception("It key have ArrayMap");
        } else {
            for (int binCount = 0; ; ++binCount) {
                if ((p = e.next) == null) {
                    e.next = new Node<>(hash, key, value);
                    size++;
                    break;
                }
                if (p.hash == hash && ((k = e.getKey()) == key || (key != null && key.equals(k))))
                    break;
                e = p;
            }
        }
    }

    private void ensureCapa() {
        if (size == table.length) {
            int newSize = table.length * 2;
            table = Arrays.copyOf(table, newSize);
        }
    }

    @Override
    public void set(Entity<K, V> entry) throws Exception {
        K key = entry.getKey();
        V value = entry.getValue();
        set(key, value);
    }

    @Override
    public Entity<K, V> remove(K key) {
        int hash = hash(key);
        int index = getIndex(hash);

        Node<K, V> p;
        Node<K, V> node = null;
        Node<K, V> e;

        Entity<K, V> entity = null;
        if (table != null && table.length > 0 && (p = table[index]) != null) {
            if (p.hash == hash && (p.getKey() == key || key.equals(p.getKey()))) {
                node = p;
            } else if ((e = p.next) != null) {
                do {
                    if (e.hash == hash && (e.getKey() == key || key.equals(e.getKey()))) {
                        node = e;
                        break;
                    }
                    p = e;
                } while ((e = e.next) != null);
            }
            if (node == p)
                table[index] = node.next;
            else
                p.next = node.next;
            --size;
            return node;
        }
        return null;
    }

    @Override
    public Entity<K, V> remove(Entity<K, V> e) {
        return remove(e.getKey());
    }

    @Override
    public List getKeys() {
        List<K> result = new ArrayList<K>();
        Node<K, V>[] tab;
        if (table != null && !isEmpty()) {
            for (int i = 0; i < table.length; ++i) {
                for (Node<K, V> e = table[i]; e != null; e = e.next) {
                    result.add(e.getKey());
                }
            }
            return result;
        }
        return null;
    }

    @Override
    public List getValues() {
        List<V> result = new ArrayList();
        Node<K, V>[] tab;
        if (table != null && !isEmpty()) {
            for (int i = 0; i < table.length; ++i) {
                for (Node<K, V> e = table[i]; e != null; e = e.next) {
                    result.add(e.getValue());
                }
            }
            return result;
        }
        return null;
    }

    @Override
    public V get(K key) {
        Node<K, V> node = getNode(hash(key), key);
        if (node != null)
            return node.getValue();
        return null;
    }

    final Node<K, V> getNode(int hash, Object key) {
        int index = getIndex(hash);
        Node<K, V> p;
        Node<K, V> e;
        if (table != null && table.length > 0 && (p = table[index]) != null) {
            if (p.hash == hash && (p.getKey() == key || key.equals(p.getKey()))) {
                return p;
            } else if ((e = p.next) != null) {
                do {
                    if (e.hash == hash && (e.getKey() == key || key.equals(e.getKey()))) {
                        return e;
                    }
                    p = e;
                } while ((e = e.next) != null);
            }
        }
        return null;
    }

    @Override
    public Entry getEntity(K key) {
        Node<K, V> node = getNode(hash(key), key);
        if (node != null)
            return new Entry(node.getKey(), node.getValue());
        return null;
    }

    @Override
    public boolean contains(V value) {
        Node<K, V>[] tab;
        V v;
        if ((tab = table) != null && size > 0) {
            for (int i = 0; i < tab.length; ++i) {
                for (Node<K, V> e = tab[i]; e != null; e = e.next) {
                    if ((v = e.getValue()) == value ||
                            (value != null && value.equals(v)))
                        return true;
                }
            }
        }
        return false;
    }

    @Override
    public int clear() {
        Node<K, V>[] tab;
        int count = size;
        if ((tab = table) != null && size > 0) {
            size = 0;
            for (int i = 0; i < tab.length; ++i)
                tab[i] = null;
        }
        return count;
    }

    @Override
    public int size() {
        return size;
    }

    static class Node<K, V> extends Entry<K, V> {
        final int hash;
        Node<K, V> next;

        Node(int hash, K key, V value) {
            super(key, value);
            this.hash = hash;
            this.next = next;
        }

        public final K getKey() {
            return super.getKey();
        }

        public final V getValue() {
            return super.getValue();
        }

        public final String toString() {
            return super.toString();
        }

        public final int hashCode() {
            return super.hashCode();
        }

        public final boolean equals(Object o) {
            if (o == this)
                return true;
            if (o instanceof Entity) {
                Entity<?, ?> e = (Entity<?, ?>) o;
                if (Objects.equals(getKey(), e.getKey()) &&
                        Objects.equals(getValue(), e.getValue()))
                    return true;
            }
            return false;
        }
    }
}