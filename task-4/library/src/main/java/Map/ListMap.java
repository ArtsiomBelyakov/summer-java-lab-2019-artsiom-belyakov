package Map;

public class ListMap<K, V> extends ArrayMap<K, V> {

    static class Entry<K, V> extends Node<K, V> {
        Entry<K, V> before, after;
        Entry(int hash, K key, V value) {
            super(hash, key, value);
        }
    }

    transient Entry<K, V> head;
    transient Entry<K, V> tail;
    
    private void linkNodeLast(Entry<K, V> p) {
        Entry<K, V> last = tail;
        tail = p;
        if (last == null)
            head = p;
        else {
            p.before = last;
            last.after = p;
        }
    }

    @Override
    public void set(K key, V value) throws Exception {
        super.set(key, value);
        Entry<K, V> p = new Entry<K, V>(hash(key), key, value);
        linkNodeLast(p);
    }

    @Override
    public int clear() {
        head = tail = null;
        return super.clear();
    }
}