package Map;

import Entity.Entity;
import List.List;

public interface Map<K, V> {
    boolean isEmpty();
    void set(K key, V value) throws Exception;
    void set(Entity<K, V> e) throws Exception;
    Entity<K, V> remove(K key);
    Entity<K, V> remove(Entity<K, V> e);
    List getKeys();
    List getValues();
    V get(K key);
    Entity<K, V> getEntity(K key);
    boolean contains(V value);
    int clear();
    int size();
}