package Queue;

import Collection.Collection;
import Iterator.Iterator;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.EmptyStackException;
import java.util.NoSuchElementException;

public class ArrayQueue<E> implements Queue {
    private final int INITIAL_SIZE = 5;

    private E[] queue;
    private int size;

    public ArrayQueue() {
        queue = (E[]) new Object[INITIAL_SIZE];
    }

    private void resize(int newLength) {
        E[] newArray = (E[]) new Object[newLength];
        System.arraycopy(queue, 0, newArray, 0, size);
        queue = newArray;
    }

    @Override
    public Boolean isEmpty() {
        return size == 0;
    }

    @Override
    public Object peek() {
        int len = size();
        if (len == 0)
            throw new EmptyStackException();
        if (len - 1 >= size) {
            throw new ArrayIndexOutOfBoundsException(len - 1 + " >= " + size);
        }
        return queue[len - 1];
    }

    @Override
    public Object poll() {
        E element=queue[size-1];
        queue[size-1]=null;
        size--;
        return element;
    }

    @Override
    public Object pull() {
        return queue[0];
    }

    @Override
    public Object remove() {
        E element = queue[0];
        for (int i = 0; i < size-1; i++)
            queue[i] = queue[i + 1];
        queue[size-1] = null;
        size--;
        return element;
    }

    @Override
    public void push(Object o) {
        if (o != null)
            if (size == queue.length) {
                resize((queue.length * 3) / 2 + 1);
                queue[size++] = (E) o;
            } else queue[size++] = (E) o;
    }

    @Override
    public void pushAll(Collection c) {
        Object[] a = c.toArray(new Object[c.size()]);
        int numNew = a.length;
        resize(size + numNew);  // Increments modCount
        System.arraycopy(a, 0, queue, size, numNew);
        size += numNew;
    }

    @Override
    public void pushAll(Object[] a) {
        int numNew = a.length;
        resize(size + numNew);  // Increments modCount
        System.arraycopy(a, 0, queue, size, numNew);
        size += numNew;
    }

    @Override
    public int search(Object o) {
        if (o == null) {
            for (int i = 0; i < size; i++) {
                if (queue[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (o.equals(queue[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public int clear() {
        int count=size();
        for (int i = 0; i < size; i++)
            queue[i] = null;
        size = 0;
        return count;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Object[] toArray(Object[] a) {
        if (a.length < size)
            return (Object[]) Arrays.copyOf(queue, size, a.getClass());
        System.arraycopy(queue, 0, a, 0, size);
        if (a.length > size)
            a[size] = null;
        return a;
    }

    private void removeElementAt(int index) {
        if (index >= size) {
            throw new ArrayIndexOutOfBoundsException(index + " >= " +
                    size);
        } else if (index < 0) {
            throw new ArrayIndexOutOfBoundsException(index);
        }
        int j = size - index - 1;
        if (j > 0) {
            System.arraycopy(queue, index + 1, queue, index, j);
        }
        size--;
        queue[size] = null; /* to let gc do its work */
    }

    @Override
    public Iterator<E> getIterator() {
        return new Itr();
    }

    private class Itr implements Iterator<E> {
        int cursor=size-1;       // index of next element to return
        int lastRet = -1; // index of last element returned; -1 if no such

        Itr() {
        }

        public boolean hasNext() {
            return cursor != -1;
        }

        @Override
        public E getNext() {
            int i=cursor;
            if(i<=-1)
                throw new NoSuchElementException();
            if (i <= -1)
                throw new ConcurrentModificationException();
            cursor = i - 1;
            return (E) queue[lastRet = i];
        }

        public void remove() {
            if (lastRet < 0)
                throw new IllegalStateException();

            try {
                ArrayQueue.this.removeElementAt(lastRet);
                cursor = lastRet;
                lastRet = -1;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        @Override
        public void addBefore(E e) {
            for (int i = size-1; i >= lastRet; i--) {
                queue[i + 1] = queue[i];
                queue[i] = e;
            }
            size++;
        }

        @Override
        public void addAfter(E e) {
            for (int i = size; i - 1 >= lastRet; i--) {
                queue[i + 1] = queue[i];
                queue[i] = e;
            }
            size++;
        }
    }
}