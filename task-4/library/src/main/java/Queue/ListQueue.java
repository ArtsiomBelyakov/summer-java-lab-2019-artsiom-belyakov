package Queue;

import Collection.Collection;
import Iterator.Iterator;
import List.LinkedList;

import java.util.EmptyStackException;
import java.util.NoSuchElementException;

public class ListQueue<E> implements Queue {

    private Node<E> first;
    private Node<E> last;
    private int size = 0;

    @Override
    public Iterator getIterator() {
        return new ListItr(0);
    }

    @Override
    public Boolean isEmpty() {
        return size == 0;
    }

    @Override
    public Object peek() {
        final Node<E> l = last;
        if (l == null)
            throw new EmptyStackException();
        return l.item;
    }

    @Override
    public Object poll() {
        final Node<E> l = last;
        if (l == null)
            throw new EmptyStackException();
        last=last.prev;
        last.next=null;
        size--;
        return l.item;
    }

    private E unlinkLast(Node<E> l) {
        // assert l == last && l != null;
        final E element = l.item;
        final Node<E> prev = l.prev;
        l.item = null;
        l.prev = null; // help GC
        last = prev;
        if (prev == null)
            first = null;
        else
            prev.next = null;
        size--;
        return element;
    }
    @Override
    public Object pull() {
        final Node<E> l = first;
        if (l == null)
            throw new EmptyStackException();
        return l.item;
    }

    @Override
    public Object remove() {
        final Node<E> l = first;
        if (l == null)
            throw new EmptyStackException();
        first=first.next;
        first.prev=null;
        size--;
        return l.item;
    }

    @Override
    public void push(Object o) {
        if (o != null) {
            final Node<E> l = last;
            final Node<E> newNode = new Node<>(l, (E) o, null);
            last = newNode;
            if (l == null)
                first = newNode;
            else
                l.next = newNode;
            size++;
        }
    }
    private boolean addAll(int index, Collection<? extends E> c) {
        Object[] a = c.toArray(new Object[c.size()]);
        int numNew = a.length;
        if (numNew == 0)
            return false;

        Node<E> pred, succ;
        if (index == size) {
            succ = null;
            pred = last;
        } else {
            succ = node(index);
            pred = succ.prev;
        }

        for (Object o : a) {
            @SuppressWarnings("unchecked") E e = (E) o;
            Node<E> newNode = new Node<>(pred, e, null);
            if (pred == null)
                first = newNode;
            else
                pred.next = newNode;
            pred = newNode;
        }

        if (succ == null) {
            last = pred;
        } else {
            pred.next = succ;
            succ.prev = pred;
        }

        size += numNew;
        return true;
    }
    @Override
    public void pushAll(Collection c) {
        addAll(size, c);
    }

    @Override
    public void pushAll(Object[] a) {
        for (int i = 0; i < a.length; i++) {
            push((E) a[i]);
        }
    }

    @Override
    public int search(Object o) {
        Node<E> elem = new Node<>(null, null, null);
        elem.item = (E) o;
        for (int i = 0; i < size; i++)
            if (node(i).item.equals(elem.item))
                return i;
        return -1;
    }

    @Override
    public int clear() {
        int deleted = size;
        first = last = null;
        size = 0;
        return deleted;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Object[] toArray(Object[] a) {
        Object[] result = new Object[size];
        int i = 0;
        for (Node<E> x = first; x != null; x = x.next)
            result[i++] = x.item;
        return result;
    }

    private static class Node<E> {
        E item;
        Node<E> next;
        Node<E> prev;

        Node(Node<E> prev, E element, Node<E> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }
    }

    private Node<E> node(int index) {
        if (index < (size >> 1)) {
            Node<E> x = first;
            for (int i = 0; i < index; i++)
                x = x.next;
            return x;
        } else {
            Node<E> x = last;
            for (int i = size - 1; i > index; i--)
                x = x.prev;
            return x;
        }
    }
    private void linkFirst(E e) {
        final Node<E> f = first;
        final Node<E> newNode = new Node<>(null, e, f);
        first = newNode;
        if (f == null)
            last = newNode;
        else
            f.prev = newNode;
        size++;
    }
    private void linkLast(E e) {
        final Node<E> l = last;
        final Node<E> newNode = new Node<>(l, e, null);
        last = newNode;
        if (l == null)
            first = newNode;
        else
            l.next = newNode;
        size++;
    }
    private E unlink(Node<E> x) {
        final E element = x.item;
        final Node<E> next = x.next;
        final Node<E> prev = x.prev;
        if (prev == null) {
            first = next;
        } else {
            prev.next = next;
            x.prev = null;
        }
        if (next == null) {
            last = prev;
        } else {
            next.prev = prev;
            x.next = null;
        }
        x.item = null;
        size--;
        return element;
    }

    private class ListItr implements Iterator<E> {
        private Node<E> lastReturned;
        private Node<E> next;
        private int nextIndex;

        ListItr(int index) {
            next = (index == size) ? null : node(index);
            nextIndex = index;
        }

        public boolean hasNext() {
            return nextIndex < size;
        }

        public E getNext() {
            if (!hasNext())
                throw new NoSuchElementException();

            lastReturned = next;
            next = next.next;
            nextIndex++;
            return lastReturned.item;
        }

        public void remove() {
            if (lastReturned == null)
                throw new IllegalStateException();

            Node<E> lastNext = lastReturned.next;
            unlink(lastReturned);
            if (next == lastReturned)
                next = lastNext;
            else
                nextIndex--;
            lastReturned = null;
        }

        @Override
        public void addBefore(E e) {
            lastReturned = null;
            if (next == null)
                linkLast(e);
            else {
                final Node<E> pred = next.prev;
                final Node<E> newNode = new Node<>(pred, e, next);
                next.prev = newNode;
                if (pred == null)
                    first = newNode;
                else
                    pred.next = newNode;
                size++;
            }
            nextIndex++;
        }

        @Override
        public void addAfter(E e) {
            if (lastReturned.prev == null)
                linkFirst(e);
            else {
                final Node<E> pred = lastReturned.prev;
                final Node<E> newNode = new Node<>(pred, e, lastReturned);
                lastReturned.prev = newNode;
                if (pred == null)
                    first = newNode;
                else
                    pred.next = newNode;
                size++;
            }
            nextIndex++;
        }
    }
}