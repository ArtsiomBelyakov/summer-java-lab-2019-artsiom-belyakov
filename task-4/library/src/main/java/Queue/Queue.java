package Queue;

import Collection.Collection;
import Iterator.Iterator;

public interface Queue<E> extends Collection {
    Iterator getIterator();
    Boolean isEmpty();
    E peek();
    E poll();
    E pull();
    E remove();
    void push(E e);
    void pushAll(Collection<? extends E> c);
    void pushAll(E[] a);
    int search(E e);
    int clear();
    int size();
}