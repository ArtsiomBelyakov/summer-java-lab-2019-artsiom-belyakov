package Stack;

import Collection.Collection;
import Iterator.Iterator;

import java.util.*;

public class ArrayStack<E> implements Stack<E> {

    E[] elementData;
    int elementCount;

    public ArrayStack() {
        this.elementData = (E[]) new Object[10];
    }

    private void removeElementAt(int index) {
        if (index >= elementCount) {
            throw new ArrayIndexOutOfBoundsException(index + " >= " +
                    elementCount);
        } else if (index < 0) {
            throw new ArrayIndexOutOfBoundsException(index);
        }
        int j = elementCount - index - 1;
        if (j > 0) {
            System.arraycopy(elementData, index + 1, elementData, index, j);
        }
        elementCount--;
        elementData[elementCount] = null; /* to let gc do its work */
    }

    private boolean chekPush(Object o) {
        if (!isEmpty())
            for (Object obj : elementData)
                if (o == obj)
                    return false;
        return true;
    }

    @Override
    public Boolean isEmpty() {
        return elementCount == 0;
    }

    private void resize(int newLength) {
        E[] newArray = (E[]) new Object[newLength];
        System.arraycopy(elementData, 0, newArray, 0, elementCount);
        elementData = newArray;
    }

    @Override
    public E peek() {
        int len = size();
        if (len == 0)
            throw new EmptyStackException();
        if (len - 1 >= elementCount) {
            throw new ArrayIndexOutOfBoundsException(len - 1 + " >= " + elementCount);
        }
        return elementData[len - 1];
    }

    @Override
    public E pop() {
        E obj;
        int len = size();
        obj = peek();
        removeElementAt(len - 1);
        return obj;
    }

    @Override
    public void push(Object o) {
        if (o != null)
            if (chekPush(o)) {
                if (elementCount == elementData.length - 1)
                    resize((elementData.length * 3) / 2 + 1);
                elementData[elementCount++] = (E) o;
            }
    }

    @Override
    public void pushAll(Collection<? extends E> c) {
        Object[] a = c.toArray(new Object[c.size()]);
        pushAll(a);
    }

    @Override
    public void pushAll(Object[] a) {
        for (int i = 0; i < a.length; i++) {
            push(a[i]);
        }
    }

    @Override
    public int search(Object element) {
        if (element == null) {
            for (int i = 0; i < elementCount; i++) {
                if (elementData[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < elementCount; i++) {
                if (element.equals(elementData[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public int clear() {
        int deleted = elementCount;
        elementData = (E[]) new Object[10];
        elementCount = 0;
        return deleted;
    }

    @Override
    public int size() {
        return elementCount;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        if (a.length < elementCount)
            return (T[]) Arrays.copyOf(elementData, elementCount, a.getClass());
        System.arraycopy(elementData, 0, a, 0, elementCount);
        if (a.length > elementCount)
            a[elementCount] = null;
        return a;
    }

    @Override
    public void sort(Comparator<? super E> c) {
        Arrays.sort(elementData, 0, elementCount, c);
    }

    @Override
    public Iterator<E> getIterator() {
        return new Itr();
    }

    private class Itr implements Iterator<E> {
        int cursor = elementCount - 1;       // index of next element to return
        int lastRet = -1; // index of last element returned; -1 if no such

        Itr() {
        }

        public boolean hasNext() {
            return cursor != -1;
        }

        @Override
        public E getNext() {
            int i = cursor;
            if (i <= -1)
                throw new NoSuchElementException();
            if (i <= -1)
                throw new ConcurrentModificationException();
            cursor = i - 1;
            return (E) elementData[lastRet = i];
        }

        public void remove() {
            if (lastRet < 0)
                throw new IllegalStateException();

            try {
                ArrayStack.this.removeElementAt(lastRet);
                cursor = lastRet;
                lastRet = -1;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        @Override
        public void addBefore(E e) {
            for (int i = elementCount; i >= lastRet; i--) {
                elementData[i + 1] = elementData[i];
                elementData[i] = e;
            }
            elementCount++;
        }

        @Override
        public void addAfter(E e) {
            for (int i = elementCount; i - 1 >= lastRet; i--) {
                elementData[i + 1] = elementData[i];
                elementData[i] = e;
            }
            elementCount++;
        }
    }
}