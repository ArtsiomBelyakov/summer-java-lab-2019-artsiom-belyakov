package Stack;

import Collection.Collection;
import Iterator.Iterator;

import java.util.Arrays;
import java.util.Comparator;
import java.util.NoSuchElementException;

public class ListStack<E> implements Stack<E> {
    transient Node<E> first;
    transient Node<E> last;
    transient int size = 0;

    @Override
    public Boolean isEmpty() {
        return size == 0;
    }

    @Override
    public E peek() {
        final Node<E> l = last;
        return (l == null) ? null : l.item;
    }

    @Override
    public E pop() {
        final Node<E> l = last;
        return (l == null) ? null : unlinkLast(l);
    }

    private E unlinkLast(Node<E> l) {
        // assert l == last && l != null;
        final E element = l.item;
        final Node<E> prev = l.prev;
        l.item = null;
        l.prev = null; // help GC
        last = prev;
        if (prev == null)
            first = null;
        else
            prev.next = null;
        size--;
        return element;
    }

    private boolean chekPush(Object o) {
        if (!isEmpty()) {
            Node<E> elem = new Node<>(null, null, null);
            elem.item = (E) o;
            for (int i = 0; i < size; i++)
                if (node(i).item.equals(elem.item))
                    return false;
        }
        return true;
    }

    @Override
    public void push(E e) {
        if (e != null) {
            if (chekPush(e)) {
                final Node<E> l = last;
                final Node<E> newNode = new Node<>(l, e, null);
                last = newNode;
                if (l == null)
                    first = newNode;
                else
                    l.next = newNode;
                size++;
            }
        }
    }

    boolean addAll(int index, Collection<? extends E> c) {
        Object[] a = c.toArray(new Object[c.size()]);
        int numNew = a.length;
        if (numNew == 0)
            return false;

        Node<E> pred, succ;
        if (index == size) {
            succ = null;
            pred = last;
        } else {
            succ = node(index);
            pred = succ.prev;
        }

        for (Object o : a) {
            @SuppressWarnings("unchecked") E e = (E) o;
            Node<E> newNode = new Node<>(pred, e, null);
            if (pred == null)
                first = newNode;
            else
                pred.next = newNode;
            pred = newNode;
        }

        if (succ == null) {
            last = pred;
        } else {
            pred.next = succ;
            succ.prev = pred;
        }

        size += numNew;
        return true;
    }

    @Override
    public void pushAll(Collection<? extends E> c) {
        addAll(size, c);
    }

    @Override
    public void pushAll(E[] a) {
        for (int i = 0; i < a.length; i++) {
            push((E) a[i]);
        }
    }

    @Override
    public int search(E element) {
        Node<E> elem = new Node<>(null, null, null);
        elem.item = element;
        for (int i = 0; i < size; i++)
            if (node(i).item.equals(elem.item))
                return i;
        return -1;

    }

    @Override
    public int clear() {
        int deleted = size;
        for (Node<E> x = first; x != null; ) {
            Node<E> next = x.next;
            x.item = null;
            x.next = null;
            x.prev = null;
            x = next;
        }
        first = last = null;
        size = 0;
        return deleted;
    }

    @Override
    public int size() {
        return size;
    }

    void resetNode() {
        first = null;
        last = null;
        size = 0;
    }

    @Override
    public void sort(Comparator<? super E> c) {
        Object[] a = this.toArray(new Object[size]);
        Arrays.sort(a, (Comparator) c);
        resetNode();
        for (int i = 0; i < a.length; i++) {
            push((E) a[i]);
        }
    }


    public <T> T[] toArray(T[] a) {
        Object[] result = new Object[size];
        int i = 0;
        for (Node<E> x = first; x != null; x = x.next)
            result[i++] = x.item;
        return (T[]) result;
    }

    Node<E> node(int index) {
        if (index < (size >> 1)) {
            Node<E> x = first;
            for (int i = 0; i < index; i++)
                x = x.next;
            return x;
        } else {
            Node<E> x = last;
            for (int i = size - 1; i > index; i--)
                x = x.prev;
            return x;
        }
    }

    E unlink(Node<E> x) {
        final E element = x.item;
        final Node<E> next = x.next;
        final Node<E> prev = x.prev;

        if (prev == null) {
            first = next;
        } else {
            prev.next = next;
            x.prev = null;
        }

        if (next == null) {
            last = prev;
        } else {
            next.prev = prev;
            x.next = null;
        }

        x.item = null;
        size--;
        return element;
    }

    @Override
    public Iterator getIterator() {
        return new ListItr(size - 1);
    }

    private static class Node<E> {
        E item;
        Node<E> next;
        Node<E> prev;

        Node(Node<E> prev, E element, Node<E> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }
    }

    private void linkFirst(E e) {
        final Node<E> f = first;
        final Node<E> newNode = new Node<>(null, e, f);
        first = newNode;
        if (f == null)
            last = newNode;
        else
            f.prev = newNode;
        size++;
    }

    void linkLast(E e) {
        final Node<E> l = last;
        final Node<E> newNode = new Node<>(l, e, null);
        last = newNode;
        if (l == null)
            first = newNode;
        else
            l.next = newNode;
        size++;
    }

    private class ListItr implements Iterator<E> {
        private Node<E> lastReturned;
        private Node<E> next;
        private int nextIndex;

        ListItr(int index) {
            next = (index == size) ? null : node(index);
            nextIndex = index;
        }

        public boolean hasNext() {
            return nextIndex > -1;
        }

        public E getNext() {
            if (!hasNext())
                throw new NoSuchElementException();
            lastReturned = next;
            next = next.prev;
            nextIndex--;
            return lastReturned.item;
        }

        public void remove() {
            if (lastReturned == null)
                throw new IllegalStateException();

            Node<E> lastNext = lastReturned.next;
            unlink(lastReturned);
            if (next == lastReturned)
                next = lastNext;
            else
                nextIndex--;
            lastReturned = null;
        }

        @Override
        public void addBefore(E e) {
            if (lastReturned.prev == null)
                linkFirst(e);
            else {
                final Node<E> pred = lastReturned.prev;
                final Node<E> newNode = new Node<>(pred, e, lastReturned);
                lastReturned.prev = newNode;
                if (pred == null)
                    first = newNode;
                else
                    pred.next = newNode;
                size++;
            }
            nextIndex++;
        }

        @Override
        public void addAfter(E e) {
            lastReturned = null;
            if (next.next.next == null)
                linkLast(e);
            else {
                final Node<E> pred = next.next;
                final Node<E> newNode = new Node<>(pred, e, next.next.next);
                next.next.next.prev = newNode;
                if (pred == null)
                    last = newNode;
                else
                    pred.next = newNode;
                size++;
            }
            nextIndex++;
        }
    }
}