package Stack;

import Collection.Collection;
import Iterator.Iterator;
import java.util.Comparator;

public interface Stack<E> extends Collection<E> {
    Iterator getIterator();
    Boolean isEmpty();
    E peek();
    E pop();
    void push(E e);
    void pushAll(Collection<? extends E> c);
    void pushAll(E[] a);
    int search(E e);
    int clear();
    int size();
    void sort(Comparator<? super E> c);
}