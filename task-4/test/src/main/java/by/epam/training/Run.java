package by.epam.training;

import Collection.Collection;
import Iterator.Iterator;
import List.ArrayList;
import List.LinkedList;
import List.List;
import Map.ArrayMap;
import Map.ListMap;
import Map.Map;
import Queue.ArrayQueue;
import Queue.ListQueue;
import Queue.Queue;
import Stack.ArrayStack;
import Stack.ListStack;
import Stack.Stack;

public class Run {
    public static void main(String[] args) throws Exception {
        IntegerList();
        IntegerStack();
        IntegerQueue();
        IntegerMap();
        UserList();
        UserQueue();
        UserStack();
        UserMap();
    }

    public static void IntegerList() {
        Integer[] testArray = {5, 2, 11, 8};
        List<Integer> testList = new ArrayList<Integer>();
        testList.add(4);
        testList.add(9);
        testList.add(3);
        testList.add(5);

        System.out.println("Test Array [5 2 11 8]");

        List<Integer> arrayList = new ArrayList<Integer>();
        System.out.println("---\nAdd ArrayList");
        arrayList.add(3);
        arrayList.add(19);
        arrayList.add(8);
        arrayList.add(2);
        print(arrayList);

        System.out.println("---\nAddAll");
        arrayList.addAll(testArray);
        print(arrayList);

        System.out.println("---\nSize:" + arrayList.size());

        System.out.println("---\nfilterDifference");
        arrayList.filterDifference(testArray);
        print(arrayList);

        System.out.println("---\niterator addAfter 14");
        Iterator<Integer> iterator = arrayList.getIterator();
        iterator.getNext();
        iterator.getNext();
        iterator.addAfter(14);
        print(arrayList);

        System.out.println("---\niterator addBefore 41");
        iterator = arrayList.getIterator();
        iterator.getNext();
        iterator.getNext();
        iterator.addBefore(41);
        print(arrayList);

        System.out.println("---\nsetMaxSize 7");
        arrayList.setMaxSize(7);
        print(arrayList);

        System.out.println("\nRemove index 3");
        arrayList.remove(3);
        print(arrayList);

        System.out.println("---\nSet index 2, element 18");
        arrayList.set(2, 18);
        print(arrayList);

        System.out.println("---\nFind element 11");
        System.out.println("Index=" + arrayList.find(11));

        System.out.println("---\nGet element for index 1");
        System.out.println("Element=" + arrayList.get(1));

        System.out.println("---\nClear");
        arrayList.clear();
        print(arrayList);
    }
    public static void UserList() {
        User vladik=new User("Vladik","vladik@gmail.com",64, User.Sex.male);
        User irka=new User("Irka","irka@gmail.com",20, User.Sex.female);
        User dashka=new User("Dashka","dashka@gmail.com",23, User.Sex.female);
        User valerka=new User("Valerka","valerka@gmail.com",24, User.Sex.male);
        User test=new User("Test","test@gmail.com",8, User.Sex.male);

        System.out.println("---\nAdd LinkedList");
        List<User> userList=new LinkedList<User>();
        userList.add(vladik);
        userList.add(irka);
        userList.add(dashka);
        printUser(userList);


        List<User> userList2=new LinkedList<User>();
        userList2.add(valerka);


        System.out.println("---\nAddAll");
        userList.addAll(userList2);
        printUser(userList);

        System.out.println("---\nSize:" + userList.size());

        System.out.println("---\nfilterMatches");
        userList.filterMatches(userList2);
        printUser(userList);

        System.out.println("---\niterator addAfter valerka");
        Iterator<User> iterator = userList.getIterator();
        iterator.getNext();
        iterator.getNext();
        iterator.addAfter(valerka);
        printUser(userList);

        System.out.println("---\niterator addBefore test");
        iterator.addBefore(test);
        printUser(userList);

        System.out.println("---\nsetMaxSize 4");
        userList.setMaxSize(4);
        printUser(userList);

        System.out.println("\nRemove index 3");
        userList.remove(3);
        printUser(userList);

        System.out.println("---\nSet index 2, element test");
        userList.set(2, test);
        printUser(userList);

        System.out.println("---\nFind element irka");
        System.out.println("Index=" + userList.find(irka));

        System.out.println("---\nGet element for index 2");
        System.out.println("Element=" + userList.get(2));

        System.out.println("---\nClear");
        userList.clear();
        printUser(userList);
    }
    public static void IntegerQueue(){
        Queue<Integer> integerQueue = new ArrayQueue<>();
        System.out.println("---\npush ArrayQueue");
        integerQueue.push(4);
        integerQueue.push(9);
        integerQueue.push(3);
        integerQueue.push(5);
        print(integerQueue);

        List<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(3);
        arrayList.add(19);
        arrayList.add(8);
        arrayList.add(2);

        System.out.println("---\nArrayList [3 19 8 2]");

        System.out.println("---\nAddAll");
        integerQueue.pushAll(arrayList);
        print(integerQueue);

        System.out.println("---\nSize:"+integerQueue.size());
        System.out.println("---\nisEmpty:"+ integerQueue.isEmpty());

        System.out.println("---\npull");
        System.out.println( integerQueue.pull());
        print(integerQueue);

        System.out.println("---\npoll");
        System.out.println( integerQueue.poll());
        print(integerQueue);

        System.out.println("---\nremove");
        System.out.println( integerQueue.remove());
        print(integerQueue);

        System.out.println("---\niterator addAfter 14");
        Iterator<Integer> iterator=integerQueue.getIterator();
        iterator.getNext();
        iterator.getNext();
        iterator.addAfter(14);
        print(integerQueue);

        System.out.println("---\niterator addBefore 41");
        iterator.addBefore(41);
        print(integerQueue);

        System.out.println("---\nsearch element 2:");
        System.out.println( "index: "+integerQueue.search(2));

        System.out.println("---\nClear");
        integerQueue.clear();
        print(integerQueue);
    }
    public static void UserQueue(){
        User vladik=new User("Vladik","vladik@gmail.com",64, User.Sex.male);
        User irka=new User("Irka","irka@gmail.com",20, User.Sex.female);
        User dashka=new User("Dashka","dashka@gmail.com",23, User.Sex.female);
        User valerka=new User("Valerka","valerka@gmail.com",24, User.Sex.male);
        User test=new User("Test","test@gmail.com",8, User.Sex.male);

        Queue<User> userQueue = new ListQueue<>();

        System.out.println("---\npush ListQueue");
        userQueue.push(vladik);
        userQueue.push(irka);
        userQueue.push(dashka);
        printUser(userQueue);

        Queue<User> userQueue2=new ListQueue<User>();
        userQueue2.push(valerka);

        System.out.println("---\npushAll");
        userQueue.pushAll(userQueue2);
        printUser(userQueue);

        System.out.println("---\nSize:"+userQueue.size());
        System.out.println("---\nisEmpty:"+ userQueue.isEmpty());

        System.out.println("---\npull");
        System.out.println( userQueue.pull());
        printUser(userQueue);

        System.out.println("---\npoll");
        System.out.println( userQueue.poll());
        printUser(userQueue);

        System.out.println("---\nremove");
        System.out.println( userQueue.remove());
        printUser(userQueue);

        System.out.println("---\niterator addAfter test");
        Iterator<User> iterator=userQueue.getIterator();
        iterator.getNext();
        iterator.getNext();
        iterator.addAfter(test);
        printUser(userQueue);

        System.out.println("---\niterator addBefore valerka");
        iterator.addBefore(valerka);
        printUser(userQueue);

        System.out.println("---\nsearch element test:");
        System.out.println( "index: "+userQueue.search(test));

        System.out.println("---\nClear");
        userQueue.clear();
        printUser(userQueue);
    }
    public static void IntegerStack(){
        Integer[] testArray = {15, 2, 11, 8};

        Stack<Integer> integerStack = new ListStack<Integer>();
        System.out.println("---\nAdd ListStack");

        integerStack.push(4);
        integerStack.push(9);
        integerStack.push(3);
        integerStack.push(5);

        print(integerStack);
        System.out.println("---\nisEmpty: " + integerStack.isEmpty());
        System.out.println("---\nTest Array [15 2 11 8]");

        System.out.println("---\npushAll");
        integerStack.pushAll(testArray);
        print(integerStack);

        System.out.println("---\nSize:" + integerStack.size());

        System.out.println("---\npop:" + integerStack.pop());
        print(integerStack);

        System.out.println("---\npeek:" + integerStack.peek());
        print(integerStack);

        System.out.println("---\niterator addAfter 14");
        Iterator<Integer> iterator = integerStack.getIterator();
        iterator.getNext();
        iterator.getNext();
        iterator.addAfter(14);
        print(integerStack);

        System.out.println("---\niterator addBefore 41");
        iterator = integerStack.getIterator();
        iterator.getNext();
        iterator.getNext();
        iterator.addBefore(41);
        print(integerStack);

        System.out.println("---\nsearch 15");
        System.out.println("index: "+integerStack.search(15));

        System.out.println("---\nComporator");
        integerStack.sort(Integer::compareTo);
        print(integerStack);

        System.out.println("---\nClear");
        integerStack.clear();
        print(integerStack);
    }
    public static void UserStack(){
        User vladik=new User("Vladik","vladik@gmail.com",64, User.Sex.male);
        User irka=new User("Irka","irka@gmail.com",20, User.Sex.female);
        User dashka=new User("Dashka","dashka@gmail.com",23, User.Sex.female);
        User valerka=new User("Valerka","valerka@gmail.com",24, User.Sex.male);
        User test=new User("Test","test@gmail.com",8, User.Sex.male);

        Stack<User> userStack = new ArrayStack<User>();
        System.out.println("---\nAdd ArrayStack");

        userStack.push(vladik);
        userStack.push(irka);
        userStack.push(dashka);
        printUser(userStack);

        Queue<User> userQueue2=new ListQueue<User>();
        userQueue2.push(valerka);

        System.out.println("---\nisEmpty: " + userStack.isEmpty());

        System.out.println("---\npushAll");
        userStack.pushAll(userQueue2);
        printUser(userStack);

        System.out.println("---\nSize:" + userStack.size());

        System.out.println("---\npop:" + userStack.pop());
        printUser(userStack);

        System.out.println("---\npeek:" + userStack.peek());
        printUser(userStack);

        System.out.println("---\niterator addAfter test");
        Iterator<User> iterator = userStack.getIterator();
        iterator.getNext();
        iterator.getNext();
        iterator.addAfter(test);
        printUser(userStack);

        System.out.println("---\niterator addBefore valerka");
        iterator = userStack.getIterator();
        iterator.getNext();
        iterator.getNext();
        iterator.addBefore(valerka);
        printUser(userStack);

        System.out.println("---\nsearch Irka");
        System.out.println("index: "+userStack.search(irka));

        System.out.println("---\nComporator");
        userStack.sort(User::compareTo);
        printUser(userStack);

        System.out.println("---\nClear");
        userStack.clear();
        printUser(userStack);
    }
    public static void IntegerMap() throws Exception {
        Map<String,Integer> map =new ArrayMap<String, Integer>();
        System.out.println("---\nArrayMap isEmpty: "+map.isEmpty());
        map.set("sad",12);
        map.set("asd",14);
        map.set("dsc",8);

        System.out.println("---\ngetKeys: ");
        print(map.getKeys());
        System.out.println("---\ngetValues: ");
        print(map.getValues());
        System.out.println("---\ncontains 14: "+ map.contains(14));
        System.out.println("---\nGet value for key 'sad': "+ map.get("sad"));
        System.out.println("---\nsize: "+ map.size());
        System.out.println("---\nclear count: "+ map.clear());
    }
    public static void UserMap() throws Exception {
        User vladik=new User("Vladik","vladik@gmail.com",64, User.Sex.male);
        User irka=new User("Irka","irka@gmail.com",20, User.Sex.female);
        User dashka=new User("Dashka","dashka@gmail.com",23, User.Sex.female);
        User valerka=new User("Valerka","valerka@gmail.com",24, User.Sex.male);
        User test=new User("Test","test@gmail.com",8, User.Sex.male);

        Map<String,User> map =new ListMap<String, User>();
        System.out.println("---\nArrayMap isEmpty: "+map.isEmpty());
        map.set("sad",vladik);
        map.set("asd",irka);
        map.set("dsc",dashka);
        map.set("test",test);
        map.set("valerka",valerka);
        try {
            map.set("valerka",valerka);
        } catch (Exception e){
            System.out.println(e);
        }

        System.out.println("---\ngetKeys: ");
        print(map.getKeys());
        System.out.println("---\ngetValues: ");
        print(map.getValues());
        System.out.println("---\ncontains dashka: "+ map.contains(dashka));
        System.out.println("---\nGet value for key 'sad': "+ map.get("sad"));
        System.out.println("---\nremove 'test': "+ map.remove("test"));
        System.out.println("---\nsize: "+ map.size());
        System.out.println("---\nclear count: "+ map.clear());
    }

    public static void print(Collection<Integer> arrayList) {
        Iterator<Integer> iterator = arrayList.getIterator();
        while (iterator.hasNext())
            System.out.print(iterator.getNext() + " ");
        System.out.println();
    }

    public static void printUser(Collection<User> arrayList) {
        Iterator<Integer> iterator = arrayList.getIterator();
        while (iterator.hasNext())
            System.out.print(iterator.getNext() + " ");
        System.out.println();
    }
}