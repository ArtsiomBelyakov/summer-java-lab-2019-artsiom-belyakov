package by.epam.training;

public class User implements Comparable<User> {

    enum Sex {
        male, female
    }

    private String name;
    private String email;
    private int age;
    private Sex sex;

    public User(String name, String email, int age, Sex sex) {
        this.name = name;
        this.email = email;
        this.age = age;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "\nname='" + name + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                ", sex=" + sex;
    }

    @Override
    public int compareTo(User other) {
        if (this.getAge() > other.getAge())
            return 1;
        else if (this.getAge() == other.getAge())
            return 0;
        return -1;
    }
}