import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Coin {
    private static final int[] array = new int[50];

    public static void main(String[] args) {
        List<String> stringList = getFile(args[0]);

        for (String nextLine : stringList) {
            HashSet<String> dev = new HashSet<>();
            String strRow[] = nextLine.split(" ");
            List<Integer> nomList = nomL(strRow);
            int m = Integer.parseInt(strRow[0]);
            if (nomList.size() == 0)
                System.out.println(strRow[0] + "\nError: There are not coins\n");
            else if (nomList.size() == 1 && !(m % nomList.get(0) == 0) || m / nomList.get(0) == 1)
                System.out.println(strRow[0] + " " + nomList.get(0) + "\nError: No one way\n");
            else if (new HashSet<>(nomList).size() < nomList.size()) {
                System.out.print(strRow[0]);
                nomList.forEach(nom -> System.out.print(" " + nom));
                System.out.println("\nError: Duplicates\n");
            } else {
                Collections.sort(nomList);
                for (int nom : nomList) {
                    nominalSlag(Integer.parseInt(strRow[0]), nom, 0, dev, nomList);
                }
            }
            printConsole(strRow, nomList, dev);
        }
    }

    static List<String> getFile(String arg) {
        Path path = Paths.get(arg);
        List<String> stringList = null;
        try (Stream<String> lineStream = Files.lines(path)) {
            stringList = lineStream.collect(Collectors.toList());
        } catch (IOException ignored) {
            System.out.println("Incorrect path");
            System.exit(0);
        }
        return stringList;
    }

    static List nomL(String strRow[]) {
        List<Integer> nomList = new ArrayList<>();
        for (int i = 0; i < strRow.length - 1; i++) {
            nomList.add(Integer.valueOf(strRow[i + 1]));
        }
        return nomList;
    }

    static void nominalSlag(int m, int nom, int i, HashSet dev, List<Integer> nomList) {
        StringBuilder str = new StringBuilder();
        if (m < 0) return;
        if (m == 0) {
            for (int j = 0; j < i; j++) {
                str.append(array[j]);
            }
            str.reverse();
            dev.add(str.toString());
        } else {
            if (m >= nom) {
                array[i] = nom;
                nominalSlag(m - nom, nom, i + 1, dev, nomList);
            }
            if (nom > Collections.min(nomList))
                nominalSlag(m, nomList.get(nomList.indexOf(nom) - 1), i, dev, nomList);
        }
    }

    private static void printConsole(String strRow[], List nomList, HashSet<String> dev) {
        if (dev.size() != 0) {
            System.out.print(strRow[0] + " ");
            nomList.forEach(number -> System.out.print(number + " "));
            System.out.println();
            dev.stream().sorted((o1, o2) -> Integer.compare(o2.length(), o1.length())).forEach(System.out::println);
            System.out.println();
        }
    }
}