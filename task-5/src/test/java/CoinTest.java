import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

class CoinTest {
    private Coin coin;

    @Test
    void getFile() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("readme.txt").getPath());
        List<String> strings = new ArrayList<>();
        strings.add("6 1 2 3");
        strings.add("2 1");
        strings.add("4 2 1");
        strings.add("6 3 2");
        strings.add("9 4");
        strings.add("8");
        strings.add("7 7");
        Assert.assertEquals(strings, coin.getFile(file.toString()));
    }

    @Test
    void nomL() {
        String[] test = {"5", "7", "11", "1", "9"};
        List<Integer> nomList = new ArrayList<>();
        nomList.add(7);
        nomList.add(11);
        nomList.add(1);
        nomList.add(9);
        Assert.assertEquals(nomList, coin.nomL(test));
    }

    @Test
    void nominalSlag() {
        HashSet<String> dev = new HashSet();
        List<Integer> nomList = new ArrayList<>();
        HashSet<String> test = new HashSet<>();
        StringBuilder stringBuilder = new StringBuilder();
        nomList.add(2);
        nomList.add(3);
        nomList.add(2, 3);
        for (int nom : nomList) {
            coin.nominalSlag(6, nom, 0, dev, nomList);
        }
        stringBuilder.append(2);
        stringBuilder.append(2);
        stringBuilder.append(2);
        test.add(stringBuilder.toString());
        stringBuilder = new StringBuilder();
        stringBuilder.append(3);
        stringBuilder.append(3);
        test.add(stringBuilder.toString());
        coin.nominalSlag(6, 2, 0, dev, nomList);
        Assert.assertEquals(test, dev);
    }
}