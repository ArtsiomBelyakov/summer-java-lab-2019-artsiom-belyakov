package by.epam.training.bank;

import by.epam.training.bank.dao.SettingDAO;
import by.epam.training.bank.dao.factory.DAOFactory;
import by.epam.training.bank.entity.Credit;
import by.epam.training.bank.entity.Show;
import by.epam.training.bank.entity.User;

import java.io.IOException;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class BankRun {
    static List<Show> shows = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        DAOFactory daoFactory = DAOFactory.getFactory();
        SettingDAO settingDAO = daoFactory.getSettingDAO();

        Stream.of(settingDAO.getSetting()).forEach(
                f -> {
                    if (f.getUseDepartments() != null)
                        daoFactory.getTransactionFilialDAO().relocationTrans("\\db_" + f + ".json");
                    else daoFactory.getTransactionFilialDAO().getAllFilial().stream()
                            .forEach(n -> daoFactory.getTransactionFilialDAO().relocationTrans("\\" + n));
                }
        );

        daoFactory.getUserDAO().getAllUsersId(settingDAO.getSetting()).stream()
                .forEach(userId ->
                        daoFactory.getCreditDAO().getCreditForUserId(userId).forEach(credit -> fin(credit))
                );

        shows.stream()
                .sorted(new Show())
                .forEach(System.out::println);
    }

    public static void fin(Credit credit) {
        Period period = credit.getPeriod(credit.getTypePeriod());
        DAOFactory daoFactory = DAOFactory.getFactory();
        SettingDAO settingDAO = daoFactory.getSettingDAO();
        AtomicInteger countTrans = new AtomicInteger();

        while (credit.getDate().plus(period).isBefore(settingDAO.getSetting().getDateTo()) && credit.getMoney() > 0) {
            double discout = daoFactory.getDiscountsDAO().getDis(credit.getDate().plus(period));
            Period finalPeriod = period;
            daoFactory.getTransactionDAO().getTranForCredId(credit).stream()
                    .filter(t -> t.getDate().isAfter(credit.getDate()) && t.getDate().isBefore(credit.getDate().plus(finalPeriod).plusDays(1)))
                    .forEach(t -> {
                        double eventCost = daoFactory.getEventDAO()
                                .getForDate(credit.getDate(), t.getDate(), t.getCurrency());
                        if (eventCost != 0) {
                            credit.setMoney(credit.getMoney() - t.getMoney() * eventCost);
                        } else {
                            double cost = settingDAO.getCost(t.getCurrency());
                            credit.setMoney(credit.getMoney() - t.getMoney() * cost);
                        }
                        countTrans.getAndIncrement();
                        if (credit.getMoney() <= 0) {
                            credit.setMoney(0);
                            User user = DAOFactory.getFactory().getDataDAO().getData().getUsers().stream()
                                    .filter(user1 -> user1.getId() == credit.getUserId()).reduce((first, second) -> second).get();
                            show(credit, countTrans.get(), user, t.getDate().toString());
                        }
                    });
            if (credit.getRate() <= discout)
                credit.setDate(credit.getDate().plusYears(1).toString());
            else {
                credit.setMoney(credit.getMoney() + credit.getMoney() * (credit.getRate() - discout) / 100);
                credit.setDate(credit.getDate().plus(period).toString());
            }
        }
        User user = daoFactory.getUserDAO().getUserById(credit.getUserId());
        if (credit.getMoney() > 0) {
            show(credit, countTrans.get(), user, "");
        }
    }

    public static void show(Credit credit, int countT, User user, String date) {
        Show show = new Show();
        show.setCredit(credit);
        show.setUser(user);
        show.setCountT(countT);
        show.setDate(date);
        if (credit.getMoney() == 0)
            show.setStatus("DONE");
        else
            show.setStatus("IN PROGRESS");
        shows.add(show);
    }
}