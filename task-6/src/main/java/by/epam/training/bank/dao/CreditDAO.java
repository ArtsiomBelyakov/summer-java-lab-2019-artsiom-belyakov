package by.epam.training.bank.dao;

import by.epam.training.bank.entity.Credit;

import java.util.List;

public interface CreditDAO {
    List<Credit> getCreditForUserId(Long userId);
}