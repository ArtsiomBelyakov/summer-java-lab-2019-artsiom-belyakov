package by.epam.training.bank.dao;

import by.epam.training.bank.entity.Data;

public interface DataDAO {
    Data getData();
    void update(Data data);
}