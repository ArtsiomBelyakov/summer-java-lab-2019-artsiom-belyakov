package by.epam.training.bank.dao;

import by.epam.training.bank.entity.Discount;

import java.time.LocalDate;

public interface DiscountDAO {
    void registerBarnType(String discountTypeName, Class<? extends Discount> discountType);
    public double getDis(LocalDate creditDate);
}