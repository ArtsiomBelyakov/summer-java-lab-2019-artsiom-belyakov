package by.epam.training.bank.dao;

import by.epam.training.bank.enums.Currency;

import java.time.LocalDate;

public interface EventDAO {
    double getForDate(LocalDate start, LocalDate end, Currency currency);
}