package by.epam.training.bank.dao;

import by.epam.training.bank.entity.Setting;
import by.epam.training.bank.enums.Currency;

public interface SettingDAO {
    Setting getSetting();
    double getCost(Currency currency);
}