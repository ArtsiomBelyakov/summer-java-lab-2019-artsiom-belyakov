package by.epam.training.bank.dao;

import by.epam.training.bank.entity.Credit;
import by.epam.training.bank.entity.Transaction;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public interface TransactionDAO {
    List<Transaction> getTranForCredId(Credit credit);
}