package by.epam.training.bank.dao;

import java.util.ArrayList;

public interface TransactionFilialDAO {
    public void relocationTrans(String filials);
    ArrayList<String> getAllFilial();
}