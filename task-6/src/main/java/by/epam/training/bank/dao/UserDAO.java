package by.epam.training.bank.dao;

import by.epam.training.bank.entity.Setting;
import by.epam.training.bank.entity.User;

import java.util.List;

public interface UserDAO {
    User getUserById(long creditUserId);
    List<Long> getAllUsersId(Setting setting);
}