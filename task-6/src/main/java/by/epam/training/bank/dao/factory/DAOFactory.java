package by.epam.training.bank.dao.factory;

import by.epam.training.bank.dao.*;

public abstract class DAOFactory {
    public static DAOFactory getFactory() {
        return new JsonDaoFactory();
    }

    public abstract UserDAO getUserDAO();

    public abstract CreditDAO getCreditDAO();

    public abstract DiscountDAO getDiscountsDAO();

    public abstract SettingDAO getSettingDAO();

    public abstract TransactionDAO getTransactionDAO();

    public abstract DataDAO getDataDAO();

    public abstract TransactionFilialDAO getTransactionFilialDAO();

    public abstract EventDAO getEventDAO();
}