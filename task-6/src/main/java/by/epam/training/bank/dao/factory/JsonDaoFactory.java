package by.epam.training.bank.dao.factory;

import by.epam.training.bank.dao.*;
import by.epam.training.bank.dao.json.*;
import by.epam.training.bank.entity.Discount;
import by.epam.training.bank.entity.DiscountDeserializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonDaoFactory extends DAOFactory {
    private static final Gson gson;

    static {
        DiscountDeserializer deserializer = new DiscountDeserializer("type");
        gson = new GsonBuilder()
                .registerTypeAdapter(Discount.class, deserializer)
                .create();
    }

    public static Gson getGsonSource() {
        return gson;
    }

    @Override
    public UserDAO getUserDAO() {
        return new JsonUserDAO();
    }

    @Override
    public CreditDAO getCreditDAO() {
        return new JsonCreditDAO();
    }

    @Override
    public DiscountDAO getDiscountsDAO() {
        return new JsonDiscountDAO();
    }

    @Override
    public SettingDAO getSettingDAO() {
        return new JsonSettingDAO();
    }

    @Override
    public TransactionDAO getTransactionDAO() {
        return new JsonTransaction();
    }

    @Override
    public DataDAO getDataDAO() {
        return new JsonDataDAO();
    }

    @Override
    public TransactionFilialDAO getTransactionFilialDAO() {
        return new JsonTransacrionFilial();
    }

    @Override
    public EventDAO getEventDAO() {
        return new JsonEventDAO();
    }
}