package by.epam.training.bank.dao.json;

import by.epam.training.bank.dao.CreditDAO;
import by.epam.training.bank.dao.factory.DAOFactory;
import by.epam.training.bank.entity.Credit;
import by.epam.training.bank.entity.Setting;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class JsonCreditDAO implements CreditDAO {
    private static final DAOFactory daoFactory = DAOFactory.getFactory();

    @Override
    public List<Credit> getCreditForUserId(Long userId) {
        Setting setting = daoFactory.getSettingDAO().getSetting();
        LocalDate dateFrom = setting.getDateFrom();
        LocalDate dateTo = setting.getDateTo();

        return daoFactory.getDataDAO().getData().getCredits().stream()
                .filter(credit -> credit.getDate().isAfter(daoFactory.getSettingDAO().getSetting().getDateFrom())
                        && credit.getDate().isBefore(daoFactory.getSettingDAO().getSetting().getDateTo()))
                .filter(credit -> credit.getUserId() == userId)
                .collect(Collectors.toList());
    }
}