package by.epam.training.bank.dao.json;

import by.epam.training.bank.dao.DataDAO;
import by.epam.training.bank.dao.factory.JsonDaoFactory;
import by.epam.training.bank.entity.Data;
import by.epam.training.bank.entity.Transaction;
import com.google.gson.JsonIOException;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JsonDataDAO implements DataDAO {
    private final String filePath = System.getProperty("user.dir") + "\\data\\db.json";

    @Override
    public Data getData() {
        Data data = null;
        try (Stream<String> lineStream = Files.lines(Paths.get(filePath))) {
            data = JsonDaoFactory.getGsonSource().fromJson(
                    (lineStream.collect(Collectors.joining("\n"))), Data.class);
        } catch (JsonIOException | IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    @Override
    public void update(Data data) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            writer.write(JsonDaoFactory.getGsonSource().toJson(data));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}