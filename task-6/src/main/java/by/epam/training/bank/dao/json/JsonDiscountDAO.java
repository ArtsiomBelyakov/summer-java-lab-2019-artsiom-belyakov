package by.epam.training.bank.dao.json;

import by.epam.training.bank.dao.DiscountDAO;
import by.epam.training.bank.dao.factory.DAOFactory;
import by.epam.training.bank.entity.Discount;
import by.epam.training.bank.entity.DiscountDeserializer;
import by.epam.training.bank.entity.DiscountMany;
import by.epam.training.bank.entity.DiscountOne;
import by.epam.training.bank.enums.Type;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.time.LocalDate;
import java.util.concurrent.atomic.AtomicReference;

public class JsonDiscountDAO implements DiscountDAO {

    public void registerBarnType(String discountTypeName, Class<? extends Discount> discountType) {
        DiscountDeserializer deserializer = new DiscountDeserializer("type");
        deserializer.registerBarnType("ONE", DiscountOne.class);
        deserializer.registerBarnType("MANY", DiscountMany.class);
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Discount.class, deserializer)
                .create();
    }

    public double getDis(LocalDate creditDate) {
        AtomicReference<Double> resultDiscount = new AtomicReference<>((double) 0);
        DAOFactory.getFactory().getDataDAO().getData().getDiscounts().stream()
                .forEach(d -> {
                    if (d.getType().equals(Type.ONE)) {
                        if (((DiscountOne) d.getDiscountClass()).getDate().equals(creditDate))
                            resultDiscount.set(d.getDiscount());
                    } else {
                        LocalDate dateFrom = ((DiscountMany) d.getDiscountClass()).getDateFrom();
                        LocalDate dateTo = ((DiscountMany) d.getDiscountClass()).getDateTo();
                        if (creditDate.isAfter(dateFrom) && creditDate.isBefore(dateTo))
                            resultDiscount.set(d.getDiscount());
                    }
                });
        return resultDiscount.get();
    }
}