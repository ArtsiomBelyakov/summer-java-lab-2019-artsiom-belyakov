package by.epam.training.bank.dao.json;

import by.epam.training.bank.dao.EventDAO;
import by.epam.training.bank.dao.factory.DAOFactory;
import by.epam.training.bank.enums.Currency;
import by.epam.training.bank.entity.Event;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class JsonEventDAO implements EventDAO {

    @Override
    public double getForDate(LocalDate start, LocalDate end, Currency transaction) {
        List<Event> events = DAOFactory.getFactory().getDataDAO().getData().getEvents().stream()
                .filter(event -> event.getDate().isAfter(start)
                        && event.getDate().isBefore(end)).collect(Collectors.toList());
        if (events.size() != 0)
            return events.stream().
                    filter(event ->
                            event.getCurrency() == transaction)
                    .reduce((first, second) -> second).orElse(null)
                    .getCost();
        return 0;
    }
}