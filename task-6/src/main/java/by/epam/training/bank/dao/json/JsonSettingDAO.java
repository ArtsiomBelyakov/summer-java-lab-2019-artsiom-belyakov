package by.epam.training.bank.dao.json;

import by.epam.training.bank.dao.SettingDAO;
import by.epam.training.bank.dao.factory.JsonDaoFactory;
import by.epam.training.bank.entity.Setting;
import by.epam.training.bank.enums.Currency;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JsonSettingDAO implements SettingDAO {
    private final String filePath = System.getProperty("user.dir") + "\\data";

    @Override
    public Setting getSetting() {
        Setting setting = null;
        try (Stream<String> lineStream = Files.lines(Paths.get(filePath + "\\setting.json"))) {
            String json = lineStream.collect(Collectors.joining("\n"));
            setting = JsonDaoFactory.getGsonSource().fromJson(json, Setting.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return setting;
    }

    @Override
    public double getCost(Currency currency) {
        if (currency == Currency.EUR)
            return getSetting().getStartCostEUR();
        else return getSetting().getStartCostEUR();
    }
}