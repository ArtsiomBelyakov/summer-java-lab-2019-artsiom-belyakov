package by.epam.training.bank.dao.json;

import by.epam.training.bank.dao.TransactionFilialDAO;
import by.epam.training.bank.dao.factory.DAOFactory;
import by.epam.training.bank.dao.factory.JsonDaoFactory;
import by.epam.training.bank.entity.Data;
import by.epam.training.bank.entity.Transaction;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JsonTransacrionFilial implements TransactionFilialDAO {
    private final String filePath = System.getProperty("user.dir") + "\\data";

    private List<Transaction> getTransForFilial(String filials) {
        List<Transaction> transactions = new ArrayList<>();
        try (Stream<String> lineStream = Files.lines(Paths.get(filePath + filials))) {
            String json = lineStream.collect(Collectors.joining("\n"));
                Type listType = new TypeToken<ArrayList<Transaction>>() {
                }.getType();
                transactions = JsonDaoFactory.getGsonSource().fromJson(json, listType);
        } catch (NoSuchFileException e){
            System.out.println("Filial file is empty");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return transactions;
    }

    @Override
    public void relocationTrans(String filials) {
        int count = getTransForFilial(filials).size();
        while (count != 0) {
            List<Transaction> transactions = getTransForFilial(filials);
            Transaction transaction = transactions.stream()
                    .findFirst().get();
            transactions.remove(transaction);
            String json = new Gson().toJson(transactions);
            setTransInDB(filials, json);
            Data data = DAOFactory.getFactory().getDataDAO().getData();
            data.getTransactions().add(transaction);
            DAOFactory.getFactory().getDataDAO().update(data);
            count--;
        }
    }

    private void setTransInDB(String filials, String json) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath + filials))) {
            writer.write(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> getAllFilial() {
        ArrayList<String> files = new ArrayList<>();
        Arrays.stream(Objects.requireNonNull(new File(filePath).listFiles()))
                .forEach(file -> {
                    if (file.isFile())
                        if (file.getName().matches("^db_[\\w]+\\.json"))
                            files.add(file.getName());
                });
        return files;
    }
}