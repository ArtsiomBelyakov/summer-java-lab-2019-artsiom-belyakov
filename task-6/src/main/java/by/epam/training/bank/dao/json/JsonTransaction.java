package by.epam.training.bank.dao.json;

import by.epam.training.bank.dao.TransactionDAO;
import by.epam.training.bank.dao.factory.DAOFactory;
import by.epam.training.bank.entity.Credit;
import by.epam.training.bank.entity.Transaction;

import java.util.List;
import java.util.stream.Collectors;

public class JsonTransaction implements TransactionDAO {
    public List<Transaction> getTranForCredId(Credit credit) {
        return DAOFactory.getFactory().getDataDAO().getData().getTransactions().stream()
                .filter(transaction ->
                        transaction.getCreditId() == credit.getId())
                .filter(transaction ->
                        transaction.getDate().isAfter(credit.getDate()))
                .collect(Collectors.toList());
    }
}