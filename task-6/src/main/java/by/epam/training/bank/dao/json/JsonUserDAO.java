package by.epam.training.bank.dao.json;

import by.epam.training.bank.dao.UserDAO;
import by.epam.training.bank.dao.factory.DAOFactory;
import by.epam.training.bank.entity.Setting;
import by.epam.training.bank.entity.User;
import by.epam.training.bank.enums.TypeShow;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class JsonUserDAO implements UserDAO {

    public User getUserById(long creditUserId){
        return DAOFactory.getFactory().getDataDAO().getData().getUsers().stream()
                .filter(user1 -> user1.getId() == creditUserId)
                .reduce((first, second) -> second)
                .get();
    }

    public List<Long> getAllUsersId(Setting setting) {
        List<User> users = DAOFactory.getFactory().getDataDAO().getData().getUsers();
        ArrayList<Long> userIdList = new ArrayList<>();
        try {
            if (setting.getShowFor().getType() == TypeShow.ID) {
                userIdList = (ArrayList<Long>) setting.getShowFor().getUsers().stream()
                        .mapToLong(Long::parseLong).boxed()
                        .filter(u -> users.stream().anyMatch(user -> user.getId() == u))
                        .collect(Collectors.toList());
            } else {
                userIdList = (ArrayList<Long>) DAOFactory.getFactory().getDataDAO().getData().getUsers().stream()
                        .filter(user -> setting.getShowFor().getUsers().stream()
                                .anyMatch(s -> (user.getName() + " " + user.getSecondName()).equals(s)))
                        .mapToLong(value -> value.getId())
                        .boxed()
                        .filter(u -> users.stream().anyMatch(user -> user.getId() == u))
                        .collect(Collectors.toList());
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return userIdList;
    }
}