package by.epam.training.bank.entity;

import by.epam.training.bank.enums.TypePeriod;

import java.time.LocalDate;
import java.time.Period;

public class Credit {
    private final long id;
    private long userId;
    private String date;
    private TypePeriod period;
    private double money;
    private double rate;

    public Credit(long id, long userId, String date, TypePeriod period, double money, double rate) {
        this.id = id;
        this.userId = userId;
        this.date = date;
        this.period = period;
        this.money = money;
        this.rate = rate;
    }

    public long getId() {
        return id;
    }

    public long getUserId() {
        return userId;
    }

    public LocalDate getDate() {
        return LocalDate.parse(date);
    }

    public void setDate(String date) {
        this.date = date;
    }

    public TypePeriod getTypePeriod() {
        return period;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public double getRate() {
        return rate;
    }

    public Period getPeriod(TypePeriod typePeriod)
    {
        Period period = null;
        switch (typePeriod) {
            case DAY:
                period = Period.ofDays(1);
                break;
            case WEEK:
                period = Period.ofWeeks(1);
                break;
            case MONTH:
                period = Period.ofMonths(1);
                break;
            case YEAR:
                period = Period.ofYears(1);
                break;
        }
        return period;
    }

    @Override
    public String toString() {
        return "Credit{" +
                "id=" + id +
                ", userId=" + userId +
                ", date='" + date + '\'' +
                ", period=" + period +
                ", money=" + money +
                ", rate=" + rate +
                '}';
    }
}