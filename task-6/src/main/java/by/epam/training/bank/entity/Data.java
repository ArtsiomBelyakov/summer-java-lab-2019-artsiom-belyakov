package by.epam.training.bank.entity;

import java.util.List;

public class Data {
    List<User> users;
    List<Credit> credits;
    List<Discount> discounts;
    List<Event> events;
    List<Transaction> transactions;

    public List<User> getUsers() {
        return users;
    }

    public List<Credit> getCredits() {
        return credits;
    }

    public List<Discount> getDiscounts() {
        return discounts;
    }

    public List<Event> getEvents() {
        return events;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    @Override
    public String toString() {
        return "Data{" +
                "users=" + users +
                ", credits=" + credits +
                ", discounts=" + discounts +
                ", events=" + events +
                ", transactions=" + transactions +
                '}';
    }
}