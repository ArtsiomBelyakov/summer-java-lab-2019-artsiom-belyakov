package by.epam.training.bank.entity;

import by.epam.training.bank.enums.Type;

import java.time.LocalDate;

public abstract class Discount {
    private long id;
    private Type type;
    private double discount;


    public abstract double creditCost(LocalDate creditDateCost);
    public abstract Discount getDiscountClass();

    public long getId() {
        return id;
    }

    public Type getType() {
        return type;
    }

    public double getDiscount() {
        return discount;
    }
}