package by.epam.training.bank.entity;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class DiscountDeserializer implements JsonSerializer<Discount>, JsonDeserializer<Discount> {
    private String discountTypeElementName;
    private Map<String, Class<? extends Discount>> discountTypeRegistry;
    private Gson gson;

    public DiscountDeserializer(String discountTypeElementName) {
        this.discountTypeElementName = discountTypeElementName;
        this.discountTypeRegistry = new HashMap<>();
        this.gson = new Gson();
        registerBarnType("ONE", DiscountOne.class);
        registerBarnType("MANY", DiscountMany.class);
    }

    public void registerBarnType(String discountTypeName, Class<? extends Discount> discountType) {
        discountTypeRegistry.put(discountTypeName, discountType);
    }

    @Override
    public Discount deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject discountObject = jsonElement.getAsJsonObject();
        JsonElement discountTypeElement = discountObject.get(discountTypeElementName);

        Class<? extends Discount> discountType = discountTypeRegistry.get(discountTypeElement.getAsString());
        return gson.fromJson(discountObject, discountType);
    }

    @Override
    public JsonElement serialize(Discount discount, Type type, JsonSerializationContext jsonSerializationContext) {
          return jsonSerializationContext.serialize(discount);
        }
    }
