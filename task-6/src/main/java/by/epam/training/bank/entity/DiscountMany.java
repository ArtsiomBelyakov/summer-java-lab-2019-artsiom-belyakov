package by.epam.training.bank.entity;

import java.time.LocalDate;

public class DiscountMany extends Discount {
    private String dateFrom;
    private String dateTo;

    public LocalDate getDateTo() {
        return LocalDate.parse(dateTo);
    }

    public LocalDate getDateFrom() {
        return LocalDate.parse(dateFrom);
    }

    @Override
    public double creditCost(LocalDate creditDateCost) {
        if (creditDateCost.isAfter(getDateFrom()) && creditDateCost.isBefore(getDateTo()))
            return getDiscount();
        return 0;
    }

    @Override
    public Discount getDiscountClass() {
        return this;
    }
}