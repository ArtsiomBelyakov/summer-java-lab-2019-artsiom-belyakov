package by.epam.training.bank.entity;

import java.time.LocalDate;

public class DiscountOne extends Discount {
    String date;

    public LocalDate getDate() {
        return LocalDate.parse(date);
    }

    @Override
    public double creditCost(LocalDate creditDateCost) {
        if (creditDateCost.equals(getDate()))
            return getDiscount();
        return 0;
    }

    public DiscountOne getDiscountClass() {
        return this;
    }
}