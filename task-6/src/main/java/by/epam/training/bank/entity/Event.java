package by.epam.training.bank.entity;

import by.epam.training.bank.enums.Currency;

import java.time.LocalDate;

public class Event {
    private long id;
    private Currency currency;
    private double cost;
    private String date;

    public long getId() {
        return id;
    }

    public Currency getCurrency() {
        return currency;
    }

    public double getCost() {
        return cost;
    }

    public LocalDate getDate() {
        return LocalDate.parse(date);
    }
}