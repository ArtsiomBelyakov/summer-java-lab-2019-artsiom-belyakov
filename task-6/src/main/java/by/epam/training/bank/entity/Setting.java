package by.epam.training.bank.entity;

import by.epam.training.bank.enums.SortBy;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;

public class Setting {
    private String dateFrom;
    private String dateTo;
    private ShowFor showFor;
    private ArrayList<String> useDepartments;
    private SortBy sortBy;
    private double startCostEUR;
    private double startCostUSD;

    public LocalDate getDateFrom() {
        if (dateFrom==null)
            return LocalDate.of(1960,01,01);
        return LocalDate.parse(dateFrom);
    }

    public LocalDate getDateTo() {
        if (dateTo==null)
            return LocalDate.now();
        return LocalDate.parse(dateTo);
    }

    public ShowFor getShowFor() {
        return showFor;
    }

    public ArrayList<String> getUseDepartments() {
        return useDepartments;
    }

    public SortBy getSortBy() {
        return sortBy;
    }

    public double getStartCostEUR() {
        return startCostEUR;
    }

    public double getStartCostUSD() {
        return startCostUSD;
    }

    @Override
    public String toString() {
        return "Setting{" +
                "dateFrom='" + dateFrom + '\'' +
                ", dateTo='" + dateTo + '\'' +
                ", showFor=" + showFor +
                ", useDepartments=" + useDepartments +
                ", sortBy='" + sortBy + '\'' +
                ", startCostEUR=" + startCostEUR +
                ", startCostUSD=" + startCostUSD +
                '}';
    }
}