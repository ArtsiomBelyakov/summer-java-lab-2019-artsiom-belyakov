package by.epam.training.bank.entity;

import by.epam.training.bank.dao.factory.DAOFactory;

import java.util.Comparator;

public class Show implements Comparator<Show> {
    private User user;
    private Credit credit;
    private int countT;
    private String date;
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "CreditId=" + credit.getId() +
                ", UserId=" + credit.getUserId() +
                ", User=" + user.getName() + " " + user.getSecondName() +
                ", CountTrans=" + countT +
                ", DegtMoney=" + credit.getMoney() +
                ", Period=" + credit.getTypePeriod() +
                ", Status=" + status + " " + date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Credit getCredit() {
        return credit;
    }

    public void setCredit(Credit credit) {
        this.credit = credit;
    }

    public int getCountT() {
        return countT;
    }

    public void setCountT(int countT) {
        this.countT = countT;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int compare(Show a, Show b) {
        switch (DAOFactory.getFactory().getSettingDAO().getSetting().getSortBy()) {
            case NAME:
                return a.getUser().getName().toUpperCase().compareTo(b.getUser().getName().toUpperCase());
            case AGE:
                return a.getUser().getBirthday().compareTo(b.getUser().getBirthday());
            case DEBT:
                if (a.getCredit().getMoney() < b.getCredit().getMoney())
                    return -1;
                else if (b.getCredit().getMoney() < a.getCredit().getMoney())
                    return 1;
                return 0;
        }
        return 0;
    }
}