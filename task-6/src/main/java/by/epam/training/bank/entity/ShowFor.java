package by.epam.training.bank.entity;

import by.epam.training.bank.enums.TypeShow;

import java.util.ArrayList;

public class ShowFor {
    private ArrayList<String> users = new ArrayList<>();
    private TypeShow type;

    public ShowFor(TypeShow type, ArrayList<String> users) {
        this.type = type;
        this.users = users;
    }

    public ArrayList<String> getUsers() {
        return users;
    }

    public TypeShow getType() {
        return type;
    }

    @Override
    public String toString() {
        return "ShowFor{" +
                "users=" + users +
                ", type=" + type +
                '}';
    }
}