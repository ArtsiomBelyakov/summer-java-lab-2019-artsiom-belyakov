package by.epam.training.bank.entity;

import by.epam.training.bank.enums.Currency;

import java.time.LocalDate;

public class Transaction {

    private long id;
    private String date;
    private long userId;
    private long creditId;
    private Currency currency;
    private int money;

    public long getId() {
        return id;
    }

    public LocalDate getDate() {
        return LocalDate.parse(date);
    }

    public long getUserId() {
        return userId;
    }

    public long getCreditId() {
        return creditId;
    }

    public Currency getCurrency() {
        return currency;
    }

    public int getMoney() {
        return money;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", date=" + date +
                ", userId=" + userId +
                ", creditId=" + creditId +
                ", currency=" + currency +
                ", money=" + money +
                '}';
    }
}