package by.epam.training.bank.entity;

import by.epam.training.bank.enums.Sex;

import java.time.LocalDate;

public class User {
    private long id;
    private String name;
    private String secondName;
    private Sex sex;
    private String birthday;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSecondName() {
        return secondName;
    }

    public LocalDate getBirthday() {
        return LocalDate.parse(birthday);
    }
}