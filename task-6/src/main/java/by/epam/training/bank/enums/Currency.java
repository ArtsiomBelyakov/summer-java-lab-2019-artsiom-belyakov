package by.epam.training.bank.enums;

public enum Currency {
    EUR, USD
}