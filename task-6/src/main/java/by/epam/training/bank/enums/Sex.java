package by.epam.training.bank.enums;

public enum Sex {
    MALE, FEMALE, ANY
}