package by.epam.training.bank.enums;

public enum SortBy {
    NAME, DEBT, AGE
}