package by.epam.training.bank.enums;

public enum TypePeriod {
    DAY, WEEK, MONTH, YEAR
}