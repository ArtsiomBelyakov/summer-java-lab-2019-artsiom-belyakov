package by.epam.training;

public class Consumer implements Runnable {
    Mqueue mqueue;

    public Consumer(Mqueue mqueue) {
        this.mqueue = mqueue;
    }

    @Override
    public void run() {
        try {
            System.out.println("Consumer: " + Thread.currentThread().getName() + ": " + mqueue.remove());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}