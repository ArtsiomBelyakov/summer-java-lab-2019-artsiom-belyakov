package by.epam.training;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Mqueue {
    BlockingQueue<Integer> queue;
    int capasity;

    public Mqueue() {
        this.queue = new LinkedBlockingQueue<>();
    }

    public Integer add() throws InterruptedException {
        if (queue == null) {
            throw new IllegalArgumentException("Queue is not defined");
        }
        int temp = (int) (Math.random() * 100);
        queue.put(temp);
        return temp;
    }

    public Integer remove() throws InterruptedException {
        return queue.take();
    }
}