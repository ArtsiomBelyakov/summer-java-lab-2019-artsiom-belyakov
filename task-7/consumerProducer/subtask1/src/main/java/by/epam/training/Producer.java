package by.epam.training;

public class Producer implements Runnable {
    Mqueue mqueue;

    public Producer(Mqueue mqueue) {
        this.mqueue = mqueue;
    }

    @Override
    public void run() {
        try {
            System.out.println("Produced: " + Thread.currentThread().getName() + ": " + mqueue.add());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}