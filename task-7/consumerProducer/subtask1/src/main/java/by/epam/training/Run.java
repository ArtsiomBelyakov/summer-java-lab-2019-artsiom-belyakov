package by.epam.training;

import java.util.Scanner;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class Run {
    public static void main(String[] args) throws InterruptedException {
        Mqueue mqueue = new Mqueue();

        Scanner in = new Scanner(System.in);
        System.out.print("Input N Consumer: ");
        int N = in.nextInt();
        System.out.print("Input M Producer: ");
        int M = in.nextInt();

        ExecutorService producers = Executors.newFixedThreadPool(M);
        ExecutorService consumers = Executors.newFixedThreadPool(N);

        for (int i = 0; i < 100; i++) {
            producers.execute(new Producer(mqueue));
            consumers.execute(new Consumer(mqueue));
        }
        producers.shutdown();
        consumers.shutdown();
    }
}