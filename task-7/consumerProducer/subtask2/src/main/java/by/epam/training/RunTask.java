package by.epam.training;

import by.epam.training.consumers.ConsumerA;
import by.epam.training.consumers.ConsumerB;
import by.epam.training.producers.Producer;
import by.epam.training.tasks.Task;
import by.epam.training.tasks.TaskA;
import by.epam.training.tasks.TaskB;

import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class RunTask {
    public static void main(String[] args) {
        BlockingQueue<Task> queue = new LinkedBlockingQueue<>();
        Task task = new TaskB();

        Scanner in = new Scanner(System.in);
        System.out.print("Input N Consumer: ");
        int countConsumer = in.nextInt();
        System.out.print("Input M Producer: ");
        int countProducer = in.nextInt();

        ExecutorService producers = Executors.newFixedThreadPool(countProducer);
        ExecutorService consumers = Executors.newFixedThreadPool(countConsumer);
        for (int i = 0; i < 100; i++) {
            producers.execute(new Producer(queue, new TaskB()));
            if (task.getClass() == TaskA.class) {
                consumers.execute(new ConsumerA(queue, (TaskA) task));
            } else {
                consumers.execute(new ConsumerB(queue, (TaskB) task));
            }
        }
        producers.shutdown();
        consumers.shutdown();
    }
}