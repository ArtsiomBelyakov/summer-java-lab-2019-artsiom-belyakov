package by.epam.training.consumers;

import by.epam.training.tasks.Task;
import by.epam.training.tasks.TaskA;

import java.util.concurrent.BlockingQueue;

public class ConsumerA implements Runnable {
    private final BlockingQueue queue;
    private TaskA taskA;

    public ConsumerA(BlockingQueue<Task> queue, TaskA taskA) {
        this.queue = queue;
        this.taskA = taskA;
    }

    public void run() {
        try {
            taskA = (TaskA) queue.take();
            System.out.println("Consumer: " + Thread.currentThread().getName() + ": " + taskA.work());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}