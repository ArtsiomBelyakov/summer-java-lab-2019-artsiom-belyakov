package by.epam.training.consumers;

import by.epam.training.tasks.Task;
import by.epam.training.tasks.TaskB;

import java.util.concurrent.BlockingQueue;

public class ConsumerB implements Runnable{
    private final BlockingQueue queue;
    private TaskB taskB;

    public ConsumerB(BlockingQueue<Task> queue, TaskB taskB) {
        this.queue=queue;
        this.taskB=taskB;
    }

    public void run() {
        try {
            taskB= (TaskB) queue.take();
            taskB.work();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}