package by.epam.training.producers;

import by.epam.training.tasks.Task;

import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable {
    private final BlockingQueue<Task> queue;
    private final Task task;

    public Producer(BlockingQueue<Task> queue, Task task) {
        this.queue = queue;
        this.task = task;
    }

    public void run() {
        try {
            queue.put(task);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}