package by.epam.training;

import java.util.concurrent.ThreadLocalRandom;

public class Race {
    private final int n;
    private final Racer[] horse;

    public Race(int n, int distance) {
        this.n = n;
        horse = new Racer[n + 1];
        for (int i = 1; i <= n; i++) {
            int speed = ThreadLocalRandom.current().nextInt(10, 70);
            horse[i] = new Racer(speed, distance, i);
        }
    }

    public void startRace() {
        for (int i = 1; i <= n; i++) {
            horse[i].start();
        }
    }
}