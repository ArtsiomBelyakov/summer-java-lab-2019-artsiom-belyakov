package by.epam.training;

import org.apache.log4j.Logger;

public class Racer extends Thread {
    private static final Logger LOGGER = Logger.getLogger(Racer.class);
    private final int number;
    private final double speed;
    private double distance;

    Racer(int speed, int distance, int i) {
        this.number = i;
        this.speed = speed / 3.6;
        System.out.println("Horse-" + number + " speed " + speed + "km/h");
        this.distance = distance;
    }

    @Override
    public void run() {
        while (distance > 0) {
            distance = distance - speed;
            if (distance > 0)
                LOGGER.info("Horse-" + number + " to the finish " + Math.round(distance * 100.0) / 100.0 + " meters");
            else
                LOGGER.info("Horse-" + number + " FINISH");
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                LOGGER.error("InterruptedException: ", ex);
            }
        }
    }
}