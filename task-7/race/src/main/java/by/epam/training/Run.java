package by.epam.training;

import java.util.Scanner;

public class Run {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter number of horses: ");
        int number = in.nextInt();
        System.out.print("Enter distance in meters: ");
        int distance = in.nextInt();

        Race race = new Race(number, distance);
        race.startRace();
    }
}