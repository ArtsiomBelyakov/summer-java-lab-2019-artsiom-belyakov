package by.epam.training.xml;

import java.time.LocalDate;

public class Birthday {
    private int day;
    private int month;
    private int year;

    public LocalDate getBirthday()
    {
        return LocalDate.of(year,month,day);
    }

    @Override
    public String toString() {
        return "Birthday{" +
                "day=" + day +
                ", month=" + month +
                ", year=" + year +
                '}';
    }
}