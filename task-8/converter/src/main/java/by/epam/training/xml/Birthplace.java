package by.epam.training.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

public class Birthplace {
    @XStreamAlias("city")
    @XStreamAsAttribute
    String city;

    @Override
    public String toString() {
        return "Birthplace{" +
                "city='" + city + '\'' +
                '}';
    }
}