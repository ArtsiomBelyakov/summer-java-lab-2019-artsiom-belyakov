package by.epam.training.xml;


import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.extended.EncodedByteArrayConverter;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws IOException, JDOMException {
        SAXBuilder builder = new SAXBuilder();
        String filePath = System.getProperty("user.dir") + "\\people.xml";
        Document doc = builder.build(filePath);
        Element root = doc.getRootElement().clone();

        List<Element> children = root.getChildren();
        children.add(2, newPerson());

        for (int i = 3; i < children.size(); i++) {
            children.get(i).getAttribute("ID").setValue(String.valueOf(i + 1));

        }

        children.stream()
                .filter(p -> p.getChild("name").getText().substring(0, 1).equals("I"))
                .forEach(p -> p.getChild("work").setText(random()));

        Element finalRoot = new Element("people");
        children.stream()
                .forEach(el -> {
                    Element temp;
                    temp = el.clone();
                    finalRoot.addContent(temp.detach());
                });

        doc = new Document(finalRoot);

        try {
            XMLOutputter outputter = new XMLOutputter();
            outputter.setFormat(Format.getPrettyFormat());
            FileWriter fw = new FileWriter("people.xml");
            outputter.output(doc, fw);
            fw.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        List<Person> persons = null;
        try {
            persons = unmarshalling(new File(filePath));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        persons = persons.stream().sorted(Comparator.comparingInt(Person::age)).collect(Collectors.toList());
        marshaller(persons, "people");
    }

    public static String random() {
        String[] texts = {"Godel Technologies Europe", "ItSupportMe", "Anderson"};
        Random random = new Random();
        int pos = random.nextInt(texts.length);
        return texts[pos];
    }

    public static Element newPerson() {
        Element elPerson = new Element("person");
        elPerson.addContent(new Element("surname").addContent("Epamov"));
        elPerson.addContent(new Element("name").addContent("Epam"));
        elPerson.addContent(new Element("birthday")
                .addContent(new Element("day").addContent("05"))
                .addContent(new Element("month").addContent("05"))
                .addContent(new Element("year").addContent("1993")));
        elPerson.addContent(new Element("birthplace").setAttribute("city", "Rechitsa"));
        elPerson.addContent(new Element("work").addContent("Hospital"));
        elPerson.setAttribute(new Attribute("ID", "3"));
        return elPerson;
    }

    public static List<Person> unmarshalling(File file) throws IOException, ClassNotFoundException {
        XStream xStream = new XStream(new DomDriver());
        xStream.alias("people", List.class);
        xStream.alias("person", Person.class);
        xStream.aliasAttribute(Person.class, "id", "ID");
        xStream.aliasField("surname", Person.class, "surname");
        xStream.aliasField("name", Person.class, "name");
        xStream.aliasField("birthplace", Person.class, "birthplace");
        xStream.aliasAttribute(Birthplace.class, "city", "city");
        xStream.aliasField("work", Person.class, "work");
        xStream.registerConverter((Converter) new EncodedByteArrayConverter());
        return (ArrayList<Person>) xStream.fromXML(file);
    }

    public static void marshaller(List<Person> object, String nameXmlFile) throws IOException {
        XStream xStream = new XStream(new DomDriver());
        xStream.alias(nameXmlFile, List.class);
        xStream.processAnnotations(Person.class);
        String xml = xStream.toXML(object);
        System.out.println(xml);
        saveToFile(xml, nameXmlFile);
    }

    private static void saveToFile(String xml, String nameFile) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(nameFile + ".xml")));
        bufferedWriter.write(xml);
        bufferedWriter.close();
    }
}