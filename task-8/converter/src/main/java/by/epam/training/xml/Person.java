package by.epam.training.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

import java.time.LocalDate;
import java.time.Period;

@XStreamAlias("Person")
public class Person {

    @XStreamAlias("ID")
    @XStreamAsAttribute
    private int id;

    @XStreamAlias("surname")
    private String surname;

    @XStreamAlias("name")
    private String name;

    @XStreamAlias("birthday")
    private Birthday birthday;

    @XStreamAlias("birthplace")
    private Birthplace birthplace;

    @XStreamAlias("work")
    private String work;

    public Person() {
    }

    public Person(int id, String surname, String name, Birthday birthday, Birthplace birthplace, String work) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.birthday = birthday;
        this.birthplace = birthplace;
        this.work = work;
    }

    public int age() {
        return Period.between(birthday.getBirthday(), LocalDate.now()).getYears();
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", birthday=" + birthday +
                ", birthplace='" + birthplace + '\'' +
                ", work='" + work + '\'' +
                '}';
    }
}